﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using System.Collections.ObjectModel;

using FloorPlan.Features;
using FloorPlan.Interaction.CustomDialogBoxes;
using FloorPlan.Components.Main;
using FloorPlan.Logic.FileManagement;
using FloorPlan.Model.Main;
using FloorPlan.Logic.Serialization.Data;

namespace FloorPlan
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region local variables/properties

        //==========Runtime Data==========
        private CommonSettings _creationSettings;
        public CommonSettings commonSettings
        {
            get { return _creationSettings; }
            set
            {
                if (_creationSettings != value)
                {
                    _creationSettings = value;
                }
            }
        }

        private string _currentProjectPath = null;
        public string currentProjectPath
        {
            get { return _currentProjectPath; }
            set
            {
                if (this._currentProjectPath != value)
                {
                    this._currentProjectPath = value;
                    this.NotifyPropertyChanged("currentProjectPath");
                }
            }
        }

        private string _originalProjectPath = null;
        public string originalProjectPath
        {
            get { return _originalProjectPath; }
            set
            {
                if (this._originalProjectPath != value)
                {
                    this._originalProjectPath = value;
                    this.NotifyPropertyChanged("originalProjectPath");
                }
            }
        }

        private string _floorMapFileName = null; //rename to name
        public string floorMapFileName
        {
            get { return _floorMapFileName; }
            set
            {
                if (this._floorMapFileName != value)
                {
                    this._floorMapFileName = value;
                    this.NotifyPropertyChanged("floorMapFileName");
                }
            }
        }

        public ObservableCollection<Capsule> listCapsules = new ObservableCollection<Capsule>();

        //========== Editing States ==========
        private bool? _isMultipleDraggingEnabled = null;
        public bool? isMultipleDraggingEnabled
        {
            get { return _isMultipleDraggingEnabled; }
            set
            {
                if (this._isMultipleDraggingEnabled != value)
                {
                    this._isMultipleDraggingEnabled = value;

                    if (_isMultipleDraggingEnabled == true)
                    {
                        textBlockDraggingState.Foreground = Brushes.Green;
                        textBlockDraggingState.Text = "enabled";
                    }
                    else if (_isMultipleDraggingEnabled == false)
                    {
                        textBlockDraggingState.Foreground = Brushes.DarkRed;
                        textBlockDraggingState.Text = "disabled";
                    }

                    this.NotifyPropertyChanged("isMultipleDraggingEnabled");
                }
            }
        }

        public enum ModeElementInsertion
        {
            None,
            IsAddingPhoto,
            IsAddingMissingPhoto,
            IsAddingName,
            IsAddingMissingName,
            IsAddingPhotoName
        }
        private ModeElementInsertion _modeElementInserting;
        public ModeElementInsertion ModeInsertionRuntime
        {
            get { return _modeElementInserting; }
            set
            {
                if (this._modeElementInserting != value || value == ModeElementInsertion.None)
                {
                    ModeElementInsertion oldAddingMode = this._modeElementInserting;
                    this._modeElementInserting = value;
                    ModeElementInsertion newAddingMode = this._modeElementInserting;

                    this.menuItemAddPerson.Background = Brushes.Transparent;
                    this.menuItemAddPerson.Foreground = Brushes.Black;
                    if (newAddingMode == ModeElementInsertion.None)
                    {
                        gridSelectionReceiver.Visibility = Visibility.Hidden;
                        gridSelectionReceiver.Cursor = Cursors.Arrow;
                        gridSelectionReceiver.ReleaseMouseCapture();

                        if (gridArtBoard.Children.Contains(selectionBorder))
                        {
                            gridArtBoard.Children.Remove(selectionBorder);
                        }

                        this.toolBarAddingMode.Visibility = Visibility.Collapsed;
                        this.textBlockInsertingMode.Text = "";
                    }
                    else
                    {
                        if (ModeInsertionRuntime != ModeElementInsertion.IsAddingMissingPhoto && ModeInsertionRuntime != ModeElementInsertion.IsAddingMissingName)
                        {
                            this.menuItemAddPerson.Background = Brushes.Green;
                            this.menuItemAddPerson.Foreground = Brushes.White;
                        }

                        gridSelectionReceiver.Visibility = Visibility.Visible;
                        gridSelectionReceiver.Cursor = Cursors.Cross;

                        this.toolBarAddingMode.Visibility = Visibility.Visible;
                    }

                    switch (oldAddingMode)
                    {
                        case ModeElementInsertion.IsAddingPhoto:
                            this.menuItemAddPhoto.Background = Brushes.Transparent;
                            this.menuItemAddPhoto.Foreground = Brushes.Black;
                            this.menuItemAddPhoto.IsChecked = false;
                            break;
                        case ModeElementInsertion.IsAddingName:
                            this.menuItemAddName.Background = Brushes.Transparent;
                            this.menuItemAddName.Foreground = Brushes.Black;
                            this.menuItemAddName.IsChecked = false;
                            break;
                        case ModeElementInsertion.IsAddingPhotoName:
                            this.menuItemAddPhotoName.Background = Brushes.Transparent;
                            this.menuItemAddPhotoName.Foreground = Brushes.Black;
                            this.menuItemAddPhotoName.IsChecked = false;
                            break;
                        case ModeElementInsertion.IsAddingMissingName:
                            this.buttonInsertMissingName.ClearValue(BackgroundProperty);
                            this.buttonInsertMissingName.Foreground = Brushes.Black;
                            break;
                        case ModeElementInsertion.IsAddingMissingPhoto:
                            this.buttonInsertMissingPhoto.ClearValue(BackgroundProperty);
                            this.buttonInsertMissingPhoto.Foreground = Brushes.Black;
                            break;
                    }

                    switch (newAddingMode)
                    {
                        case ModeElementInsertion.IsAddingPhoto:
                            this.menuItemAddPhoto.Background = Brushes.Green;
                            this.menuItemAddPhoto.Foreground = Brushes.White;
                            this.menuItemAddPhoto.IsChecked = true;
                            this.selectedCapsule = null;
                            this.textBlockInsertingMode.Text = "Photo";
                            break;
                        case ModeElementInsertion.IsAddingMissingPhoto:
                            this.buttonInsertMissingPhoto.Background = Brushes.Green;
                            this.buttonInsertMissingPhoto.Foreground = Brushes.White;
                            this.textBlockInsertingMode.Text = "Missing Photo";
                            break;
                        case ModeElementInsertion.IsAddingName:
                            this.menuItemAddName.Background = Brushes.Green;
                            this.menuItemAddName.Foreground = Brushes.White;
                            this.menuItemAddName.IsChecked = true;
                            this.selectedCapsule = null;
                            this.textBlockInsertingMode.Text = "Name";
                            break;
                        case ModeElementInsertion.IsAddingMissingName:
                            this.buttonInsertMissingName.Background = Brushes.Green;
                            this.buttonInsertMissingName.Foreground = Brushes.White;
                            this.textBlockInsertingMode.Text = "Missing Name";
                            break;
                        case ModeElementInsertion.IsAddingPhotoName:
                            this.menuItemAddPhotoName.Background = Brushes.Green;
                            this.menuItemAddPhotoName.Foreground = Brushes.White;
                            this.menuItemAddPhotoName.IsChecked = true;
                            this.selectedCapsule = null;
                            this.textBlockInsertingMode.Text = "Photo and Name";
                            break;
                    }

                    this.NotifyPropertyChanged("ModeInsertionRuntime");
                }
            }
        }

        private Capsule _selectedCapsule;
        public Capsule selectedCapsule
        {
            get { return this._selectedCapsule; }
            set
            {
                if (this._selectedCapsule != value || value == null)
                {
                    Capsule oldSelectedCapsule = _selectedCapsule;
                    _selectedCapsule = value;

                    if (ModeInsertionRuntime != ModeElementInsertion.None && _selectedCapsule != null)
                    {
                        ModeInsertionRuntime = ModeElementInsertion.None;
                    }

                    if (oldSelectedCapsule != null)
                    {
                        if (oldSelectedCapsule.nameComponent != null)
                        {
                            oldSelectedCapsule.nameComponent.exitTextEditingMode();
                        }
                        oldSelectedCapsule.isSelected = false;
                    }

                    if (_selectedCapsule != null)
                    {
                        _selectedCapsule.isSelected = true;

                        panelForMissingPhoto.Visibility = (_selectedCapsule.photoComponent == null) ? Visibility.Visible : Visibility.Collapsed;
                        panelForMissingName.Visibility = (_selectedCapsule.nameComponent == null) ? Visibility.Visible : Visibility.Collapsed;
                        panelForPhoto.Visibility = (_selectedCapsule.photoComponent == null) ? Visibility.Collapsed : Visibility.Visible;
                        panelForName.Visibility = (_selectedCapsule.nameComponent == null) ? Visibility.Collapsed : Visibility.Visible;

                        toolBarDraggingMode.Visibility = (_selectedCapsule.nameComponent != null && _selectedCapsule.photoComponent != null) ? Visibility.Visible : Visibility.Collapsed;
                    }
                    else
                    {
                        toolBarDraggingMode.Visibility = Visibility.Collapsed;
                        panelForMissingPhoto.Visibility = Visibility.Collapsed;
                        panelForMissingName.Visibility = Visibility.Collapsed;
                        panelForPhoto.Visibility = Visibility.Collapsed;
                        panelForName.Visibility = Visibility.Collapsed;
                    }

                    this.NotifyPropertyChanged("selectedCapsule");
                }
            }
        }

        //====Scrolling with mouse====
        private Point? mouseFirstDragPoint = null;
        private Point? mousePrevDragPoint = null;

        //====Selection + Timer Scroll====
        public TimerScrolling timerScrolling;
        private Border selectionBorder;

        private Point? mouseDownSelecting = null;
        //=========================
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            
            commonSettings = ProjectManipulation.loadCommonSettings();
            if (commonSettings == null)
            {
                commonSettings = new CommonSettings();
                commonSettings.componentCreationSettings = new ComponentStyle();
            }

            initProperties();

            ModeInsertionRuntime = ModeElementInsertion.None;
            selectedCapsule = null;
        }
        private void initProperties()
        {
            isMultipleDraggingEnabled = true;
            ReusableFunctions.fillComboBoxWithFonts(cmbFontFamilies);

            NameScope.SetNameScope((ContextMenu)this.Resources["contextMenuName"], NameScope.GetNameScope(this)); //For MenuItem binding to work properly
            NameScope.SetNameScope((ContextMenu)this.Resources["contextMenuPhoto"], NameScope.GetNameScope(this));

            Panel.SetZIndex(gridSelectionReceiver, int.MaxValue);
            gridSelectionReceiver.Visibility = Visibility.Collapsed;
            
            timerScrolling = new TimerScrolling(scrollViewerContent, gridArtBoard);
            selectionBorder = (Border)gridArtBoard.Resources["selectionBorder"];
            RenderOptions.SetBitmapScalingMode(gridArtBoard, BitmapScalingMode.Fant); //For better scaling quality
            RenderOptions.SetBitmapScalingMode(listBoxCapsules, BitmapScalingMode.Fant); //For better scaling quality

            listBoxCapsules.ItemsSource = listCapsules;
            listBoxCapsules.Items.IsLiveSorting = true;
            listBoxCapsules.Items.SortDescriptions.Add(new SortDescription("nameComponent.text", ListSortDirection.Ascending));
            listBoxCapsules.SelectionChanged += handleListBoxCapsules_SelectionChanged;
        }

        private void WinMain_Loaded(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(currentProjectPath))
            {
                DlgProjectOptionsPrompt promptDlg = new DlgProjectOptionsPrompt();
                promptDlg.projectOwner = this;
                promptDlg.Owner = this;
                promptDlg.ShowDialog();
            }
        }
        private void WinMain_Closing(object sender, CancelEventArgs e)
        {
            if (suggestSavingAndContinue() == false)
            {
                e.Cancel = true;
            }

            if (e.Cancel == false)
            {
                ProjectManipulation.saveCommonSettings(this.commonSettings);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        private void winMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                if (ModeInsertionRuntime != ModeElementInsertion.None)
                {
                    ModeInsertionRuntime = ModeElementInsertion.None;
                }
                else if (selectedCapsule != null)
                {
                    selectedCapsule = null;
                }
            }
            else if ((e.Key == Key.LeftShift || e.Key == Key.RightShift) && isMultipleDraggingEnabled == true)
            {
                isMultipleDraggingEnabled = false;
            }
        }
        private void winMain_KeyUp(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.LeftShift || e.Key == Key.RightShift) && isMultipleDraggingEnabled == false)
            {
                isMultipleDraggingEnabled = true;
            }
        }

        private void menuItemSaveAs_Click(object sender, RoutedEventArgs e)
        {

        }
        private void menuItemSettings_Click(object sender, RoutedEventArgs e)
        {
            if (this.commonSettings != null)
            {
                CommonSettings copy = this.commonSettings.clone();
                if (copy != null)
                {
                    DlgRuntimeSettingsEditor settingsDlg = new DlgRuntimeSettingsEditor(copy);
                    settingsDlg.Owner = this;
                    settingsDlg.ShowDialog();
                    if (settingsDlg.DialogResult == true && settingsDlg.style != null)
                    {
                        this.commonSettings = copy;
                    }
                }
            }
        }

        //========== Photo Editing ===========
        //====================================
        private void photoLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectedCapsule.photoComponent != null)
            {
                //selectedCapsule.photoComponent.BringIntoView(); //The element remains at the border
                scrollToElement(selectedCapsule.photoComponent); //The element is positioned at centre
            }
        }
        private void photoChangeBorderColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                photoChangeBorderColor();
            }
        }
        private void buttonPhotoMoreOptions_Click(object sender, RoutedEventArgs e)
        {
            ContextMenu cm = this.FindResource("contextMenuPhoto") as ContextMenu;
            if (cm != null && selectedCapsule.photoComponent != null)
            {
                cm.DataContext = selectedCapsule.photoComponent;
                cm.PlacementTarget = buttonPhotoMoreOptions;
                cm.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                cm.IsOpen = true;
            }
        }

        private void menuItemPhotoBorderColorChange_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.photoComponent == ((ContextMenu)Resources["contextMenuPhoto"]).DataContext)
            {
                photoChangeBorderColor();
            }
        }
        private void menuItemPhotoChangeImage_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.photoComponent == ((ContextMenu)Resources["contextMenuPhoto"]).DataContext)
            {
                Microsoft.Win32.OpenFileDialog openFileDlg = ReusableDialogBoxes.openFileDlgImages;
                if (openFileDlg.ShowDialog(this) == true)
                {
                    BitmapSource bmpSource = ImageManipulation.extractImageFromFile(openFileDlg.FileName, true);
                    if (bmpSource != null)
                    {
                        string newPhotoFilePath = generateNewPhotoFilePath(openFileDlg.FileName);
                        selectedCapsule.photoComponent.setNewPhotoDeleteOld(newPhotoFilePath, openFileDlg.FileName, bmpSource);
                    }
                }
            }
        }
        private void menuItemPhotoFileSize_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.photoComponent == ((ContextMenu)Resources["contextMenuPhoto"]).DataContext)
            {
                if (selectedCapsule.photoComponent != null && selectedCapsule.photoComponent.imagePhotoSource != null)
                {
                    //=====Takes DPI into account=====
                    selectedCapsule.photoComponent.Width = ((BitmapSource)selectedCapsule.photoComponent.imagePhotoSource).Width + 2 * DynamicModel.RESIZABLE_BORDER_THICKNESS;
                    selectedCapsule.photoComponent.Height = ((BitmapSource)selectedCapsule.photoComponent.imagePhotoSource).Height + 2 * DynamicModel.RESIZABLE_BORDER_THICKNESS;
                    //================================

                    //selectedCapsule.photoComponent.Width = ((BitmapImage)selectedCapsule.photoComponent.imagePhotoSource).PixelWidth + 2 * PhotoComponent.RESIZABLE_BORDER_THICKNESS;
                    //selectedCapsule.photoComponent.Height = ((BitmapImage)selectedCapsule.photoComponent.imagePhotoSource).PixelHeight + 2 * PhotoComponent.RESIZABLE_BORDER_THICKNESS;
                }
            }
        }

        private void photoChangeBorderColor()
        {
            if (selectedCapsule.photoComponent != null && selectedCapsule.photoComponent.BorderBrush is SolidColorBrush)
            {
                ReusableFunctions.colorDynamicChange(selectedCapsule.photoComponent.BorderBrush as SolidColorBrush, this);
            }
        }

        //========== Name Editing ============
        //====================================
        private void nameLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectedCapsule.nameComponent != null)
            {
                //selectedCapsule.nameComponent.BringIntoView(); //The element remains at the border
                scrollToElement(selectedCapsule.nameComponent); //Centres on this element
            }
        }
        private void nameChangeBorderColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                nameChangeBorderColor();
            }
        }
        private void nameChangeForeColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                nameChangeForeColor();
            }
        }
        private void nameChangeBackColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                nameChangeBackColor();
            }
        }
        private void buttonNameToggleFontWeight_Click(object sender, RoutedEventArgs e)
        {
            selectedCapsule.nameComponent.toggleBold();
            updateNameFontToggleButtons();
        }
        private void buttonNameToggleFontStyle_Click(object sender, RoutedEventArgs e)
        {
            selectedCapsule.nameComponent.toggleItalic();
            updateNameFontToggleButtons();
        }
        private void buttonNameToggleUnderline_Click(object sender, RoutedEventArgs e)
        {
            selectedCapsule.nameComponent.toggleUnderline();
            updateNameFontToggleButtons();
        }
        private void gridNameAlignment_BorderMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                Border border = e.Source as Border;
                if (border != null && selectedCapsule != null && selectedCapsule.nameComponent != null)
                {
                    selectedCapsule.nameComponent.horizontalTextAlignment = (HorizontalAlignment)Grid.GetColumn(border);
                    selectedCapsule.nameComponent.verticalTextAlignment = (VerticalAlignment)Grid.GetRow(border);
                    updateNameAlignmentGrid();
                }
            }
        }
        private void gridNameAlignment_BorderMouseEnter(object sender, RoutedEventArgs e)
        {
            Border border = e.Source as Border;
            if (border != null && selectedCapsule != null && selectedCapsule.nameComponent != null)
            {
                if (Grid.GetColumn(border) != (int)selectedCapsule.nameComponent.horizontalTextAlignment || Grid.GetRow(border) != (int)selectedCapsule.nameComponent.verticalTextAlignment)
                {
                    border.Background = Brushes.LightBlue;
                }
            }
        }
        private void gridNameAlignment_BorderMouseLeave(object sender, RoutedEventArgs e)
        {
            Border border = e.Source as Border;
            if (border != null && selectedCapsule != null && selectedCapsule.nameComponent != null)
            {
                if (Grid.GetColumn(border) != (int)selectedCapsule.nameComponent.horizontalTextAlignment || Grid.GetRow(border) != (int)selectedCapsule.nameComponent.verticalTextAlignment)
                {
                    border.Background = Brushes.White;
                }
            }
        }
        private void buttonNameMoreOptions_Click(object sender, RoutedEventArgs e)
        {
            ContextMenu cm = this.FindResource("contextMenuName") as ContextMenu;

            if (cm != null && selectedCapsule.nameComponent != null)
            {
                cm.DataContext = selectedCapsule.nameComponent;
                cm.PlacementTarget = buttonNameMoreOptions;
                cm.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                cm.IsOpen = true;
            }
        }

        private void menuItemNameBorderColorChange_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                nameChangeBorderColor();
            }
        }
        private void menuItemNameForeColorChange_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                nameChangeForeColor();
            }
        }
        private void menuItemNameBackColorChange_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                nameChangeBackColor();
            }
        }
        private void menuItemNameEditText_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                if (selectedCapsule.nameComponent.isInTextEditingMode)
                {
                    selectedCapsule.nameComponent.exitTextEditingMode();
                }
                else
                {
                    selectedCapsule.nameComponent.enterTextEditingMode();
                }
            }
        }
        private void menuItemNameBackColorTransparent_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                selectedCapsule.nameComponent.Background = new SolidColorBrush(Colors.Transparent);
            }
        }
        private void menuItemNameDefaultSize_Click(object sender, RoutedEventArgs e)
        {
            if (selectedCapsule.nameComponent == ((ContextMenu)Resources["contextMenuName"]).DataContext)
            {
                selectedCapsule.nameComponent.autoAdjustSize();
            }
        }

        private void nameChangeBorderColor()
        {
            if (selectedCapsule.nameComponent != null && selectedCapsule.nameComponent.BorderBrush is SolidColorBrush)
            {
                ReusableFunctions.colorDynamicChange(selectedCapsule.nameComponent.BorderBrush as SolidColorBrush, this);
            }
        }
        private void nameChangeForeColor()
        {
            if (selectedCapsule.nameComponent != null && selectedCapsule.nameComponent.Foreground is SolidColorBrush)
            {
                ReusableFunctions.colorDynamicChange(selectedCapsule.nameComponent.Foreground as SolidColorBrush, this);
            }
        }
        private void nameChangeBackColor()
        {
            if (selectedCapsule.nameComponent != null && selectedCapsule.nameComponent.Background is SolidColorBrush)
            {
                ReusableFunctions.colorDynamicChange(selectedCapsule.nameComponent.Background as SolidColorBrush, this);
            }
        }

        //========= Generic Editing ==========
        //====================================
        public void requestPhotoContextMenu(PhotoComponent photoModel)
        {
            ContextMenu cm = this.FindResource("contextMenuPhoto") as ContextMenu;
            if (cm != null)
            {
                cm.DataContext = photoModel;
                cm.PlacementTarget = photoModel;
                cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                cm.IsOpen = true;
            }
        }
        public void requestNameContextMenu(NameComponent nameModel)
        {
            ContextMenu cm = this.FindResource("contextMenuName") as ContextMenu;
            if (cm != null)
            {
                cm.DataContext = nameModel;
                cm.PlacementTarget = nameModel;
                cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
                cm.IsOpen = true;
            }
        }

        private void menuItemBringToFront_Click(object sender, RoutedEventArgs e)
        {
            UIElement target = tryCastDataContext(e.Source);
            if (target == selectedCapsule.nameComponent || target == selectedCapsule.photoComponent)
            {
                bringToFront(target);
            }
        }
        private void menuItemBringForward_Click(object sender, RoutedEventArgs e)
        {
            DynamicModel target = tryCastDataContext(e.Source);
            if (target != null)
            {
                bringForward(target);
            }
        }
        private void menuItemSendBackward_Click(object sender, RoutedEventArgs e)
        {
            DynamicModel target = tryCastDataContext(e.Source);
            if (target != null)
            {
                sendBackward(target);
            }
        }
        private void menuItemSendToBack_Click(object sender, RoutedEventArgs e)
        {
            DynamicModel target = tryCastDataContext(e.Source);
            if (target != null)
            {
                sendToBack(target);
            }
        }
        private void menuItemDelete_Click(object sender, RoutedEventArgs e)
        {
            DynamicModel target = tryCastDataContext(e.Source);
            if (target != null)
            {
                selectedCapsule.DeleteComponent(target);
                updateSelectedCapsule();
            }
        }

        private DynamicModel tryCastDataContext(object element)
        {
            if (element == null)
            {
                return null;
            }
            FrameworkElement fe = element as FrameworkElement;
            DynamicModel target = fe.DataContext as DynamicModel;
            if (target == selectedCapsule.nameComponent || target == selectedCapsule.photoComponent)
            {
                return target;
            }
            else
            {
                return null;
            }
        }

        private void menuItemPhotoClose_Click(object sender, RoutedEventArgs e)
        {
            ((ContextMenu)Resources["contextMenuPhoto"]).IsOpen = false;
        }
        private void menuItemNameClose_Click(object sender, RoutedEventArgs e)
        {
            ((ContextMenu)Resources["contextMenuName"]).IsOpen = false;
        }
        //====================================

        private void scrollViewerContent_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (sliderZoom.IsEnabled == false)
            {
                return;
            }

            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                if (e.Delta > 0)
                {
                    int currentIndex = sliderZoom.Ticks.IndexOf(sliderZoom.Value);
                    if (currentIndex + 1 <= sliderZoom.Ticks.Count - 1)
                    {
                        sliderZoom.Value = sliderZoom.Ticks[currentIndex + 1];
                    }
                }
                else
                {
                    int currentIndex = sliderZoom.Ticks.IndexOf(sliderZoom.Value);
                    if (currentIndex - 1 >= 0)
                    {
                        sliderZoom.Value = sliderZoom.Ticks[currentIndex - 1];
                    }
                }

                e.Handled = true;
            }
        }
        private void sliderZoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Point desiredScaleOrigin;
            Point actualScaleOrigin;

            if (gridArtBoard.IsMouseOver)
            {
                desiredScaleOrigin = Mouse.PrimaryDevice.GetPosition(gridArtBoard);

                ((ScaleTransform)gridArtBoard.LayoutTransform).ScaleX = sliderZoom.Value;
                ((ScaleTransform)gridArtBoard.LayoutTransform).ScaleY = sliderZoom.Value;
                gridArtBoard.UpdateLayout();

                actualScaleOrigin = Mouse.PrimaryDevice.GetPosition(gridArtBoard);
            }
            else
            {
                Point centreScrollViewer = new Point(scrollViewerContent.ViewportWidth / 2, scrollViewerContent.ViewportHeight / 2);
                desiredScaleOrigin = scrollViewerContent.TranslatePoint(centreScrollViewer, gridArtBoard);

                ((ScaleTransform)gridArtBoard.LayoutTransform).ScaleX = sliderZoom.Value;
                ((ScaleTransform)gridArtBoard.LayoutTransform).ScaleY = sliderZoom.Value;
                gridArtBoard.UpdateLayout();

                centreScrollViewer = new Point(scrollViewerContent.ViewportWidth / 2, scrollViewerContent.ViewportHeight / 2);
                actualScaleOrigin = scrollViewerContent.TranslatePoint(centreScrollViewer, gridArtBoard);
            }

            double offsetX = scrollViewerContent.HorizontalOffset + (desiredScaleOrigin.X - actualScaleOrigin.X) * (scrollViewerContent.ExtentWidth / gridArtBoard.ActualWidth);
            double offsetY = scrollViewerContent.VerticalOffset + (desiredScaleOrigin.Y - actualScaleOrigin.Y) * (scrollViewerContent.ExtentHeight / gridArtBoard.ActualHeight);

            if (!double.IsNaN(offsetX) && !double.IsNaN(offsetY))
            {
                scrollViewerContent.ScrollToHorizontalOffset(offsetX);
                scrollViewerContent.ScrollToVerticalOffset(offsetY);
            }
        }

        private void gridScalingSpaceProvider_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ModeInsertionRuntime == ModeElementInsertion.None)
            {
                Point mousePos = e.GetPosition(scrollViewerContent);
                if (mousePos.X <= scrollViewerContent.ViewportWidth && mousePos.Y <= scrollViewerContent.ViewportHeight)
                {
                    mouseFirstDragPoint = mousePos;
                    mousePrevDragPoint = mousePos;
                    Mouse.Capture(gridScalingSpaceProvider);
                }
            }
        }
        private void gridScalingSpaceProvider_MouseMove(object sender, MouseEventArgs e)
        {
            if (mousePrevDragPoint.HasValue)
            {
                Point mouseMoveDragPoint = e.GetPosition(scrollViewerContent);

                scrollViewerContent.ScrollToHorizontalOffset(scrollViewerContent.HorizontalOffset - (mouseMoveDragPoint.X - mousePrevDragPoint.Value.X));
                scrollViewerContent.ScrollToVerticalOffset(scrollViewerContent.VerticalOffset - (mouseMoveDragPoint.Y - mousePrevDragPoint.Value.Y));

                mousePrevDragPoint = mouseMoveDragPoint;
            }
        }
        private void gridScalingSpaceProvider_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (mousePrevDragPoint.HasValue)
            {
                gridScalingSpaceProvider.ReleaseMouseCapture();

                if (mouseFirstDragPoint == mousePrevDragPoint)
                {
                    selectedCapsule = null;
                }

                mouseFirstDragPoint = null;
                mousePrevDragPoint = null;
            }
        }

        private void gridSelectionReceiver_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ModeInsertionRuntime != ModeElementInsertion.None)
            {
                ModeInsertionRuntime = ModeElementInsertion.None;
                e.Handled = true;
            }
        }
        private void gridSelectionReceiver_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ModeInsertionRuntime != ModeElementInsertion.None)
            {
                mouseDownSelecting = e.GetPosition(imageFloorMap);

                //=Adjust selection area=
                selectionBorder.Margin = new Thickness(mouseDownSelecting.Value.X, mouseDownSelecting.Value.Y, 0, 0);
                selectionBorder.Width = 0;
                selectionBorder.Height = 0;

                if (ModeInsertionRuntime == ModeElementInsertion.IsAddingName || ModeInsertionRuntime == ModeElementInsertion.IsAddingMissingName)
                {
                    insertElement(getInsertionArea(selectionBorder), ModeInsertionRuntime);
                    mouseDownSelecting = null;
                }
                else
                {
                    Mouse.Capture(gridSelectionReceiver);
                    if (!gridArtBoard.Children.Contains(selectionBorder))
                    {
                        gridArtBoard.Children.Add(selectionBorder);
                    }
                }
                e.Handled = true;
            }
        }
        private void gridSelectionReceiver_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDownSelecting.HasValue &&
                (ModeInsertionRuntime == ModeElementInsertion.IsAddingPhoto ||
                ModeInsertionRuntime == ModeElementInsertion.IsAddingMissingPhoto ||
                ModeInsertionRuntime == ModeElementInsertion.IsAddingPhotoName))
            {
                updateSelectionArea((Point)mouseDownSelecting, e.GetPosition(imageFloorMap));
                timerScrolling.Start();
                e.Handled = true;
            }
        }
        private void gridSelectionReceiver_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (mouseDownSelecting.HasValue)
            {
                gridSelectionReceiver.ReleaseMouseCapture();

                insertElement(getInsertionArea(selectionBorder), ModeInsertionRuntime);
                mouseDownSelecting = null;
            }

            e.Handled = true;
        }

        private void buttonAddMissingPhoto_Click(object sender, RoutedEventArgs e)
        {
            ModeInsertionRuntime = (ModeInsertionRuntime != ModeElementInsertion.IsAddingMissingPhoto) ? ModeElementInsertion.IsAddingMissingPhoto : ModeElementInsertion.None;
        }
        private void buttonAddMissingName_Click(object sender, RoutedEventArgs e)
        {
            ModeInsertionRuntime = (ModeInsertionRuntime != ModeElementInsertion.IsAddingMissingName) ? ModeElementInsertion.IsAddingMissingName : ModeElementInsertion.None;
        }

        private void commandImportImage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.currentProjectPath != null)
            {
                e.CanExecute = true;
            }
        }
        private void commandImportImage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            importImage();
        }
        public void importImage()
        {
            try
            {
                Microsoft.Win32.OpenFileDialog openFileDlg = ReusableDialogBoxes.openFileDlgImages;
                if (openFileDlg.ShowDialog(this) == true)
                {
                    BitmapSource bmpSource = ImageManipulation.extractImageFromFile(openFileDlg.FileName, true);
                    if (bmpSource != null)
                    {
                        imageFloorMap.Source = bmpSource;

                        string newFilePath = currentProjectPath + "\\" + ProjectManipulation.FOLDER_WITH_IMAGES + "\\" + DateTime.Now.Ticks.ToString() + System.IO.Path.GetExtension(openFileDlg.FileName);
                        File.Copy(openFileDlg.FileName, newFilePath);

                        string currentFloorMapPath = this.currentProjectPath + "\\" + ProjectManipulation.FOLDER_WITH_IMAGES + "\\" + this.floorMapFileName;
                        if (File.Exists(currentFloorMapPath))
                        {
                            File.Delete(currentFloorMapPath);
                        }
                        this.floorMapFileName = System.IO.Path.GetFileName(newFilePath);

                        resetView();
                    }
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
        private void commandExportImage_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (imageFloorMap.Source == null) ? false : true;
        }
        private void commandExportImage_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            BitmapSource bmpImage = (BitmapSource)imageFloorMap.Source;

            if (bmpImage != null)
            {
                try
                {
                    BitmapSource bmpFromVisual = extractBitmapFromVisual(gridArtBoard, bmpImage.DpiX, bmpImage.DpiY);

                    DlgImageExport imageExportDlg = new DlgImageExport();
                    imageExportDlg.Owner = this;
                    imageExportDlg.imageToExport = bmpFromVisual;
                    imageExportDlg.ShowDialog();
                }
                catch (Exception ex)
                {
                    ReusableFunctions.showCommonError(ex);
                }
            }
            else
            {
                MessageBox.Show("No image to save.", "No image", MessageBoxButton.OK);
            }
        }
        private void commandNewProject_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void commandNewProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!suggestSavingAndContinue())
            {
                return;
            }

            createNewProject();
        }
        public bool createNewProject()
        {
            bool wasCreated = ProjectManipulation.createNewProject(this);
            if (wasCreated)
            {
                if (MessageBox.Show("A new project created!\n\nWould you like to import a floor map?", "Success", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    importImage();
                }
                return true;
            }
            return false;
        }
        private void commandOpenProject_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        private void commandOpenProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!suggestSavingAndContinue())
            {
                return;
            }

            bool projectWasOpened = ProjectManipulation.openProject(this, this);
            if (projectWasOpened)
            {
                resetView();
            }
        }
        private void commandSaveProject_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.currentProjectPath != null)
            {
                e.CanExecute = true;
            }
        }
        private void commandSaveProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            bool wasSaved = saveCurrentProject();
            if (wasSaved)
            {
                MessageBox.Show("Project has been saved.", "Saving Status", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        private void commandAddPhoto_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (imageFloorMap.Source == null)
            {
                e.CanExecute = false;
            }
            else
            {
                e.CanExecute = true;
            }
        }
        private void commandAddPhoto_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ModeInsertionRuntime != ModeElementInsertion.IsAddingPhoto)
            {
                ModeInsertionRuntime = ModeElementInsertion.IsAddingPhoto;
            }
            else
            {
                ModeInsertionRuntime = ModeElementInsertion.None;
            }
        }
        private void commandAddName_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (imageFloorMap.Source == null)
            {
                e.CanExecute = false;
            }
            else
            {
                e.CanExecute = true;
            }
        }
        private void commandAddName_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ModeInsertionRuntime != ModeElementInsertion.IsAddingName)
            {
                ModeInsertionRuntime = ModeElementInsertion.IsAddingName;
            }
            else
            {
                ModeInsertionRuntime = ModeElementInsertion.None;
            }
        }
        private void commandAddPhotoName_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (imageFloorMap.Source == null)
            {
                e.CanExecute = false;
            }
            else
            {
                e.CanExecute = true;
            }
        }
        private void commandAddPhotoName_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ModeInsertionRuntime != ModeElementInsertion.IsAddingPhotoName)
            {
                ModeInsertionRuntime = ModeElementInsertion.IsAddingPhotoName;
            }
            else
            {
                ModeInsertionRuntime = ModeElementInsertion.None;
            }
        }

        private void ButtonDeleteSelectedCapsules_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (listBoxCapsules.SelectedItems.Count > 0)
                {
                    MessageBoxResult result = MessageBox.Show("Do you want to delete highlighted components from the list?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Yes)
                    {
                        Capsule[] selectedCapsules = new Capsule[listBoxCapsules.SelectedItems.Count];
                        for (int i = 0; i < selectedCapsules.Length; i++)
                        {
                            selectedCapsules[i] = (Capsule)listBoxCapsules.SelectedItems[i];
                        }

                        listBoxCapsules.UnselectAll();

                        foreach (Capsule capsule in selectedCapsules)
                        {
                            capsule.Delete();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
        private void ButtonChangeSelectedCapsules_Click(object sender, RoutedEventArgs e)
        {

        }
        private void ButtonFindComponentImage_Click(object sender, RoutedEventArgs e)
        {
            Capsule capsuleFromList = listBoxCapsules.SelectedItem as Capsule;
            if (listBoxCapsules.SelectedItems.Count == 1 && capsuleFromList != null && capsuleFromList.photoComponent != null)
            {
                scrollToElement(capsuleFromList.photoComponent);
                this.selectedCapsule = capsuleFromList;
            }
        }
        private void ButtonFindComponentName_Click(object sender, RoutedEventArgs e)
        {
            Capsule capsuleFromList = listBoxCapsules.SelectedItem as Capsule;
            if (listBoxCapsules.SelectedItems.Count == 1 && capsuleFromList != null && capsuleFromList.nameComponent != null)
            {
                scrollToElement(capsuleFromList.nameComponent);
                this.selectedCapsule = capsuleFromList;
            }
        }

        private void expanderForCapsules_Expanded(object sender, RoutedEventArgs e)
        {
            columnListBoxCapsules.MinWidth = 190;
            columnListBoxCapsules.Width = new GridLength(190d);
        }
        private void expanderForCapsules_Collapsed(object sender, RoutedEventArgs e)
        {
            columnListBoxCapsules.MinWidth = 0;
            columnListBoxCapsules.Width = GridLength.Auto;
        }

        private BitmapSource extractBitmapFromVisual(UIElement target, double dpiX, double dpiY)
        {
            // from https://blogs.msdn.microsoft.com/jaimer/2009/07/03/rendertargetbitmap-tips/
            if (target == null)
            {
                return null;
            }

            ModeInsertionRuntime = ModeElementInsertion.None;
            selectedCapsule = null;

            //double savedScaleFactor = ((ScaleTransform)gridArtBoard.LayoutTransform).ScaleX;
            //double savedXScrollOffset = scrollViewerContent.HorizontalOffset;
            //double savedYScrollOffset = scrollViewerContent.VerticalOffset;

            //((ScaleTransform)gridArtBoard.LayoutTransform).ScaleX = 1.0;
            //((ScaleTransform)gridArtBoard.LayoutTransform).ScaleY = 1.0;

            Rect dimensions = VisualTreeHelper.GetDescendantBounds(imageFloorMap);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)(dimensions.Width * dpiX / 96.0),
                                                            (int)(dimensions.Height * dpiY / 96.0),
                                                            dpiX,
                                                            dpiY,
                                                            PixelFormats.Pbgra32);

            //====lossy rendering====
            //DrawingVisual dv = new DrawingVisual();
            //using (DrawingContext ctx = dv.RenderOpen())
            //{
            //    ctx.DrawImage(imageFloorMap.Source, new Rect(new Point(), dimensions.Size));
            //}
            //rtb.Render(dv);
            //=======================

            //====lossless rendering???====
            //target.Measure(dimensions.Size);
            //target.Arrange(new Rect(dimensions.Size));
            //rtb.Render(target);

            foreach (FrameworkElement el in gridArtBoard.Children)
            {
                if (el is DynamicModel)
                {
                    ((DynamicModel)el).changeBorderDecoratorEdgeMode(EdgeMode.Unspecified);
                    rtb.Render(el);
                    ((DynamicModel)el).changeBorderDecoratorEdgeMode(EdgeMode.Aliased);
                }
                else
                {
                    rtb.Render(el);
                }
            }
            //=======================

            //====lossy rendering 3 (the worst of all)====
            //DrawingVisual dv = new DrawingVisual();
            //using (DrawingContext ctx = dv.RenderOpen())
            //{
            //    ctx.DrawRectangle(new VisualBrush(target), null, new Rect(dimensions.Size));
            //}
            //rtb.Render(dv);
            //=======================

            //target.Visibility = Visibility.Collapsed;
            //target.Visibility = Visibility.Visible;
            //((ScaleTransform)gridArtBoard.LayoutTransform).ScaleX = savedScaleFactor;
            //((ScaleTransform)gridArtBoard.LayoutTransform).ScaleY = savedScaleFactor;
            //scrollViewerContent.ScrollToHorizontalOffset(savedXScrollOffset);
            //scrollViewerContent.ScrollToVerticalOffset(savedYScrollOffset);

            return rtb;
        }
        private void insertElement(Rect insertionArea, ModeElementInsertion ModeInsertionCaptured)
        {
            ModeInsertionRuntime = ModeElementInsertion.None;
            try
            {
                if (ModeInsertionCaptured == ModeElementInsertion.IsAddingPhoto || ModeInsertionCaptured == ModeElementInsertion.IsAddingMissingPhoto || ModeInsertionCaptured == ModeElementInsertion.IsAddingPhotoName)
                {
                    Microsoft.Win32.OpenFileDialog openFileDlg = ReusableDialogBoxes.openFileDlgImages;

                    if (openFileDlg.ShowDialog(this) == true)
                    {
                        BitmapSource bmpSource = ImageManipulation.extractImageFromFile(openFileDlg.FileName, true);

                        if (bmpSource != null)
                        {
                            PhotoComponent photoComponent = new PhotoComponent(imageFloorMap, insertionArea.Size, insertionArea.Location, commonSettings.componentCreationSettings);

                            string newPhotoFilePath = generateNewPhotoFilePath(openFileDlg.FileName);
                            photoComponent.setNewPhotoDeleteOld(newPhotoFilePath, openFileDlg.FileName, bmpSource);

                            if (ModeInsertionCaptured == ModeElementInsertion.IsAddingPhotoName)
                            {
                                double offsetY = insertionArea.Location.Y;
                                offsetY += (photoComponent.Height < photoComponent.MinHeight) ? photoComponent.MinHeight : photoComponent.Height;
                                NameComponent nameComponent = new NameComponent(imageFloorMap, new Point(insertionArea.Location.X, offsetY), commonSettings.componentCreationSettings);

                                Capsule capsule = new Capsule(this, gridArtBoard, photoComponent, nameComponent);
                                capsule.addComponentsToGrid();
                                this.selectedCapsule = capsule;
                            }
                            else if (ModeInsertionCaptured == ModeElementInsertion.IsAddingMissingPhoto && this.selectedCapsule.photoComponent == null)
                            {
                                this.selectedCapsule.photoComponent = photoComponent;
                                this.selectedCapsule.addComponentsToGrid();
                                updateSelectedCapsule();
                            }
                            else
                            {
                                Capsule capsule = new Capsule(this, gridArtBoard, photoComponent, null);
                                capsule.addComponentsToGrid();
                                this.selectedCapsule = capsule;
                            }
                        }

                    }
                }
                else if (ModeInsertionCaptured == ModeElementInsertion.IsAddingName || ModeInsertionCaptured == ModeElementInsertion.IsAddingMissingName)
                {
                    //====Create Name Label Object====
                    NameComponent nameComponent = new NameComponent(imageFloorMap, insertionArea.Location, commonSettings.componentCreationSettings);

                    if (ModeInsertionCaptured == ModeElementInsertion.IsAddingMissingName && this.selectedCapsule.nameComponent == null)
                    {
                        this.selectedCapsule.nameComponent = nameComponent;
                        this.selectedCapsule.addComponentsToGrid();
                        updateSelectedCapsule();
                    }
                    else
                    {
                        Capsule capsule = new Capsule(this, gridArtBoard, null, nameComponent);
                        capsule.addComponentsToGrid();
                        this.selectedCapsule = capsule;
                    }
                    //===========================
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
        private void updateSelectedCapsule()
        {
            //====Reselect selected capsule
            Capsule capsule = this.selectedCapsule;
            this.selectedCapsule = null;
            this.selectedCapsule = capsule;
            handleListBoxCapsules_SelectionChanged(null, null); //Enables/disables buttons for finding added components
        }
        private void bringToFront(UIElement element)
        {
            if (element != null && gridArtBoard.Children.Contains(element))
            {
                gridArtBoard.Children.Remove(element);
                gridArtBoard.Children.Add(element);
            }
        }
        private void sendToBack(UIElement element)
        {
            if (element != null && gridArtBoard.Children.Contains(element))
            {
                gridArtBoard.Children.Remove(element);
                gridArtBoard.Children.Insert(2, element); //First two elements are grid (0) and image (1)
            }
        }
        private void bringForward(UIElement element)
        {
            if (element != null && gridArtBoard.Children.Contains(element))
            {
                int indexOfElement = gridArtBoard.Children.IndexOf(element);

                if (indexOfElement < gridArtBoard.Children.Count - 1)
                {
                    gridArtBoard.Children.Remove(element);
                    gridArtBoard.Children.Insert(indexOfElement + 1, element);
                }
            }
        }
        private void sendBackward(UIElement element)
        {
            if (element != null && gridArtBoard.Children.Contains(element))
            {
                int indexOfElement = gridArtBoard.Children.IndexOf(element);

                if (indexOfElement > 2) //First two elements are grid (0) and image (1)
                {
                    gridArtBoard.Children.Remove(element);
                    gridArtBoard.Children.Insert(indexOfElement - 1, element);
                }
            }
        }
        private void scrollToElement(FrameworkElement element)
        {
            if (element != null && gridArtBoard.Children.Contains(element))
            {
                double scalingRatio = (scrollViewerContent.ExtentWidth / gridArtBoard.ActualWidth);

                Point view = new Point(
                    (element.Margin.Left + element.ActualWidth / 2) * scalingRatio,
                    (element.Margin.Top + element.ActualHeight / 2) * scalingRatio);

                scrollViewerContent.ScrollToHorizontalOffset(view.X - (scrollViewerContent.ViewportWidth) / 2);
                scrollViewerContent.ScrollToVerticalOffset(view.Y - (scrollViewerContent.ViewportHeight) / 2);
            }
        }
        private void resetView()
        {
            //====Adjust view on screen====
            sliderZoom.Value = 1.0;
            scrollViewerContent.ScrollToLeftEnd();
            scrollViewerContent.ScrollToTop();
            //=============================
        }
        private void updateNameAlignmentGrid()
        {
            if (selectedCapsule != null && selectedCapsule.nameComponent != null)
            {
                foreach (Border border in gridNameAlignment.Children)
                {
                    if (Grid.GetColumn(border) == (int)selectedCapsule.nameComponent.horizontalTextAlignment && Grid.GetRow(border) == (int)selectedCapsule.nameComponent.verticalTextAlignment)
                    {
                        border.Background = Brushes.LightGreen;
                    }
                    else
                    {
                        border.Background = Brushes.White;
                    }
                }
            }
            else
            {
                foreach (Border border in gridNameAlignment.Children)
                {
                    border.Background = Brushes.White;
                }
            }
        }
        private void updateNameFontToggleButtons()
        {
            if (selectedCapsule != null && selectedCapsule.nameComponent != null)
            {
                buttonNameToggleFontWeight.Foreground = (selectedCapsule.nameComponent.textBoxFontWeight == FontWeights.Bold) ? Brushes.LimeGreen : Brushes.Black;
                buttonNameToggleFontStyle.Foreground = (selectedCapsule.nameComponent.textBoxFontStyle == FontStyles.Italic) ? Brushes.LimeGreen : Brushes.Black;
                buttonNameToggleUnderline.Foreground = (selectedCapsule.nameComponent.isTextUnderlined) ? Brushes.LimeGreen : Brushes.Black;
            }
        }
        private string generateNewPhotoFilePath(string originalFileName)
        {
            string newFileName = String.Format("{0}\\{1}\\{2}{3}",
                this.currentProjectPath,
                ProjectManipulation.FOLDER_WITH_PHOTOS,
                DateTime.Now.Ticks.ToString(),
                System.IO.Path.GetExtension(originalFileName));

            return newFileName;
        }
        private bool suggestSavingAndContinue()
        {
            bool moveOn = false;

            if (currentProjectPath != null)
            {
                MessageBoxResult result = MessageBox.Show("Any unsaved changes will be lost.\n\nWould you like to save your current project?", "Save", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.Yes:
                        bool wasSaved = saveCurrentProject();
                        moveOn = (wasSaved) ? true : false;
                        break;
                    case MessageBoxResult.No:
                        moveOn = true;
                        break;
                    default:
                        moveOn = false;
                        break;
                }
            }
            else
            {
                moveOn = true;
            }

            return moveOn;
        }
        private bool saveCurrentProject()
        {
            bool wasSaved = false;

            try
            {
                bool oldDataDeleted = ReusableFunctions.deleteFolder(this.originalProjectPath);
                if (!oldDataDeleted)
                {
                    wasSaved = false;
                }
                else
                {
                    ProjectManipulation.saveProject(this, this.currentProjectPath, System.IO.Path.GetFileName(this.originalProjectPath));
                    ReusableFunctions.directoryCopy(this.currentProjectPath, this.originalProjectPath, true);
                    wasSaved = true;
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
                wasSaved = false;
            }
            return wasSaved;
        }
        private void updateSelectionArea(Point mouseDownSelecting, Point mouseMoveSelecting)
        {
            Rect rectSelection = new Rect(mouseDownSelecting, mouseMoveSelecting);

            if (rectSelection.Left < 0)
            {
                rectSelection.X = 0;
                rectSelection.Width = selectionBorder.ActualWidth + selectionBorder.Margin.Left;
            }
            if (rectSelection.Top < 0)
            {
                rectSelection.Y = 0;
                rectSelection.Height = selectionBorder.ActualHeight + selectionBorder.Margin.Top;
            }
            if (rectSelection.Width + rectSelection.Left > imageFloorMap.ActualWidth)
            {
                rectSelection.Width = imageFloorMap.ActualWidth - rectSelection.Left;
            }
            if (rectSelection.Height + rectSelection.Top > imageFloorMap.ActualHeight)
            {
                rectSelection.Height = imageFloorMap.ActualHeight - rectSelection.Top;
            }

            selectionBorder.Margin = new Thickness(rectSelection.Left, rectSelection.Top, 0, 0);
            selectionBorder.Width = rectSelection.Width;
            selectionBorder.Height = rectSelection.Height;
        }
        private Rect getInsertionArea(Border selectionBorder)
        {
            Rect insertionArea = new Rect(new Point(selectionBorder.Margin.Left, selectionBorder.Margin.Top), new Size(selectionBorder.Width, selectionBorder.Height));
            return insertionArea;
        }
        private void handleListBoxCapsules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bool btImageEnable = false;
            bool btNameEnabled = false;

            if (listBoxCapsules.SelectedItems.Count == 1)
            {
                Capsule selectedItem = listBoxCapsules.SelectedItem as Capsule;

                if (selectedItem != null)
                {
                    if (selectedItem.photoComponent != null)
                    {
                        btImageEnable = true;
                    }
                    if (selectedItem.nameComponent != null)
                    {
                        btNameEnabled = true;
                    }
                }
            }

            btFindImage.IsEnabled = btImageEnable;
            btFindName.IsEnabled = btNameEnabled;
        }

        public void updateComponentStates()
        {
            updateNameAlignmentGrid();
            updateNameFontToggleButtons();
        }
        public void clearWindow()
        {
            ModeInsertionRuntime = ModeElementInsertion.None;
            selectedCapsule = null;

            foreach (Capsule capsule in listCapsules)
            {
                if (capsule.photoComponent != null && gridArtBoard.Children.Contains(capsule.photoComponent))
                {
                    gridArtBoard.Children.Remove(capsule.photoComponent);
                }

                if (capsule.nameComponent != null && gridArtBoard.Children.Contains(capsule.nameComponent))
                {
                    gridArtBoard.Children.Remove(capsule.nameComponent);
                }
            }

            listCapsules.Clear();
            currentProjectPath = null;
            floorMapFileName = null;

            imageFloorMap.Source = null;
        }
    }
}
