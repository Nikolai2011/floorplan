﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Data;
using System.IO;
using System.ComponentModel;
//using Microsoft.Windows.Themes;

using FloorPlan.Model.Main;
using FloorPlan.Components.Thumbs.MoveThumbs;
using FloorPlan.Components.Thumbs.ResizeThumbs;
using FloorPlan.Logic.Serialization.Data;
using FloorPlan.Features;

namespace FloorPlan.Components.Main
{
    public class PhotoComponent : DynamicModel, INotifyPropertyChanged
    {
        public const double MIN_SIZE = 40.0;
        public Capsule capsule;

        private Image imagePhoto;
        private string _photoFileName = null;

        public PhotoComponent(FrameworkElement moveBounds, Size initialSize, Point initialPosition)
        {
            initComponents(moveBounds);
            initCrucialProperties(initialSize, initialPosition);
            loadComponents();
        }
        public PhotoComponent(FrameworkElement moveBounds, Size initialSize, Point initialPosition, ComponentStyle defaultLookSettings)
        {
            initComponents(moveBounds);
            initCrucialProperties(initialSize, initialPosition);

            if (defaultLookSettings != null)
            {
                applyDefaultProperties(initialSize, defaultLookSettings);
            }

            loadComponents();
        }

        public new bool isSelected
        {
            get { return base.isSelected; }
            set
            {
                base.isSelected = value;
            }
        }
        public string photoFileName
        {
            get { return _photoFileName; }
        }
        public ImageSource imagePhotoSource
        {
            get { return imagePhoto.Source; }
            set
            {
                if (this.imagePhoto.Source != value)
                {
                    this.imagePhoto.Source = value;
                    this.NotifyPropertyChanged("imagePhotoSource");
                }
            }
        }
        public Brush borderDecoratorBackground
        {
            set
            {
                if (borderDecorator.Background != value)
                {
                    borderDecorator.Background = value;
                }
            }
        }

        public void setPathToPhoto(string photoFilePath, ImageSource imageFromFile)
        {
            if (File.Exists(photoFilePath))
            {
                _photoFileName = Path.GetFileName(photoFilePath);
            }

            if (imageFromFile != null)
            {
                this.borderDecoratorBackground = Brushes.Transparent;
            }
            this.imagePhotoSource = imageFromFile;
        }
        public void setNewPhotoDeleteOld(string newPhotoPath, string originalImageFilePath, ImageSource imageFromFile)
        {
            if (string.IsNullOrEmpty(originalImageFilePath) || imageFromFile == null)
            {
                return;
            }

            if (File.Exists(originalImageFilePath))
            {
                File.Copy(originalImageFilePath, newPhotoPath);

                string currentPhotoPath = Path.GetDirectoryName(newPhotoPath) + "\\" + this._photoFileName;
                if (File.Exists(currentPhotoPath))
                {
                    File.Delete(currentPhotoPath);
                }

                this._photoFileName = Path.GetFileName(newPhotoPath);
            }

            this.borderDecoratorBackground = Brushes.Transparent; //On project open, missing images are black. This statement returns transparency
            this.imagePhotoSource = imageFromFile;
        }
        public void Delete()
        {
            string currentPhotoPath = this.capsule.hostWindow.currentProjectPath + "\\" + Logic.FileManagement.ProjectManipulation.FOLDER_WITH_PHOTOS + "\\" + _photoFileName;
            if (File.Exists(currentPhotoPath))
            {
                File.Delete(currentPhotoPath);
            }
            this.imagePhotoSource = null;
        }

        private void initComponents(FrameworkElement moveBounds)
        {
            gridTopContainer = new Grid();
            borderDecorator = new Border() { Margin = new Thickness(RESIZABLE_BORDER_THICKNESS) };
            imagePhoto = new Image() { Stretch = Stretch.Fill };
            base.initSelectionBorder();
            initResizeHandles(moveBounds);
            initRectDecorators();
            thumbMove = new ThumbMovePhoto(this, moveBounds) { Opacity = 0.0, Margin = new Thickness(RESIZABLE_BORDER_THICKNESS) };
        }
        private void loadComponents()
        {
            //==========Layout===========
            gridTopContainer.Children.Add(base.borderSelection);

            borderDecorator.Child = imagePhoto;
            gridTopContainer.Children.Add(borderDecorator);

            base.addRectDecorators(gridTopContainer);
            base.addResizeHandles(gridTopContainer);
            gridTopContainer.Children.Add(thumbMove);

            this.Content = gridTopContainer;
            //===========================
        }
        private void initResizeHandles(FrameworkElement bounds)
        {
            thumbTop = new ResizeThumbPhoto(this, bounds);
            thumbLeft = new ResizeThumbPhoto(this, bounds);
            thumbRight = new ResizeThumbPhoto(this, bounds);
            thumbBottom = new ResizeThumbPhoto(this, bounds);
            thumbTopLeft = new ResizeThumbPhoto(this, bounds);
            thumbTopRight = new ResizeThumbPhoto(this, bounds);
            thumbBottomLeft = new ResizeThumbPhoto(this, bounds);
            thumbBottomRight = new ResizeThumbPhoto(this, bounds);

            base.initResizeHandlesProperties();
        }
        private void initCrucialProperties(Size initialSize, Point initialPosition)
        {
            this.HorizontalAlignment = HorizontalAlignment.Left;
            this.VerticalAlignment = VerticalAlignment.Top;
            this.MinWidth = MIN_SIZE;
            this.MinHeight = MIN_SIZE;
            this.Margin = new Thickness(initialPosition.X - RESIZABLE_BORDER_THICKNESS, initialPosition.Y - RESIZABLE_BORDER_THICKNESS, 0, 0);
            if (!initialSize.IsEmpty)
            {
                this.Width = initialSize.Width + RESIZABLE_BORDER_THICKNESS * 2;
                this.Height = initialSize.Height + RESIZABLE_BORDER_THICKNESS * 2;
            }
            this.MouseRightButtonDown += photoModel_MouseRightButtonDown;
            this.PreviewMouseLeftButtonDown += PhotoComponent_PreviewMouseLeftButtonDown;

            Binding bindingBorderThickness = new Binding("BorderThickness");
            bindingBorderThickness.Mode = BindingMode.OneWay;
            bindingBorderThickness.Source = this;
            borderDecorator.SetBinding(Border.BorderThicknessProperty, bindingBorderThickness);

            Binding bindingBorderBrush = new Binding("BorderBrush");
            bindingBorderBrush.Mode = BindingMode.OneWay;
            bindingBorderBrush.Source = this;
            borderDecorator.SetBinding(Border.BorderBrushProperty, bindingBorderBrush);

            changeBorderDecoratorEdgeMode(EdgeMode.Aliased); //Fixes the gap between a border and its content
        }

        private void applyDefaultProperties(Size initialSize, ComponentStyle defaultLookSettings)
        {
            this.BorderThickness = defaultLookSettings.photoBorderThickness;
            this.BorderBrush = new SolidColorBrush(defaultLookSettings.photoBorderColor);
            if (defaultLookSettings.photoInitialSize.IsEmpty == false)
            {
                if (initialSize.Width < this.MinWidth && initialSize.Height < this.MinHeight)
                {
                    this.Width = defaultLookSettings.photoInitialSize.Width + RESIZABLE_BORDER_THICKNESS * 2;
                    this.Height = defaultLookSettings.photoInitialSize.Height + RESIZABLE_BORDER_THICKNESS * 2;
                }
            }
        }

        private void PhotoComponent_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (capsule.nameComponent != null)
            {
                capsule.nameComponent.exitTextEditingMode();
            }
        }
        private void photoModel_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            capsule.hostWindow.selectedCapsule = capsule;
            capsule.hostWindow.requestPhotoContextMenu(this);
            e.Handled = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
    }
}
