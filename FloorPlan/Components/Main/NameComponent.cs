﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.ComponentModel;
using System.Windows.Input;

using FloorPlan.Model.Main;
using FloorPlan.Components.Thumbs.MoveThumbs;
using FloorPlan.Components.Thumbs.ResizeThumbs;
using FloorPlan.Logic.Serialization.Data;
using FloorPlan.Features;

namespace FloorPlan.Components.Main
{
    public class NameComponent : DynamicModel, INotifyPropertyChanged
    {
        public Capsule capsule;
        private TextBox textBoxWithName;

        public NameComponent(FrameworkElement moveBounds, Point initialPosition)
        {
            initComponents(moveBounds);
            initCrucialProperties(initialPosition);
            loadComponents();
        }
        public NameComponent(FrameworkElement moveBounds, Point initialPosition, ComponentStyle defaultLookSettings)
        {
            initComponents(moveBounds);
            initCrucialProperties(initialPosition);

            if (defaultLookSettings != null)
            {
                applyDefaultProperties(defaultLookSettings);
            }

            loadComponents();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        public new bool isSelected
        {
            get { return base.isSelected; }
            set
            {
                base.isSelected = value;
                capsule.hostWindow.updateComponentStates();
            }
        }
        public string text
        {
            get
            {

                return FloorPlan.Features.ReusableFunctions.PlaceOnOneLine(textBoxWithName.Text);
            }
            set
            {
                if (textBoxWithName.Text != value)
                {
                    textBoxWithName.Text = value;
                    this.NotifyPropertyChanged("text");
                }
            }
        }
        public Thickness borderThickness
        {
            get { return this.borderDecorator.BorderThickness; }
            set
            {
                if (borderDecorator.BorderThickness != value)
                {
                    borderDecorator.BorderThickness = value;

                    //Fixes incorrect alignment of small text when border thickness is changed
                    HorizontalAlignment temp = horizontalTextAlignment;
                    horizontalTextAlignment = HorizontalAlignment.Left;
                    horizontalTextAlignment = temp;

                    this.NotifyPropertyChanged("borderThickness");
                }
            }
        }
        public HorizontalAlignment horizontalTextAlignment
        {
            get { return textBoxWithName.HorizontalContentAlignment; }
            set
            {
                if (textBoxWithName.HorizontalContentAlignment != value)
                {
                    textBoxWithName.HorizontalContentAlignment = value;
                }
            }
        }
        public VerticalAlignment verticalTextAlignment
        {
            get { return textBoxWithName.VerticalContentAlignment; }
            set
            {
                if (textBoxWithName.VerticalContentAlignment != value)
                {
                    textBoxWithName.VerticalContentAlignment = value;
                }
            }
        }
        public bool isTextUnderlined
        {
            get
            {
                if (textBoxWithName.TextDecorations == null)
                {
                    return false;
                }

                bool underlined = false;
                foreach (TextDecoration decor in textBoxWithName.TextDecorations)
                {
                    if (decor.Location == TextDecorationLocation.Underline)
                    {
                        underlined = true;
                    }
                }

                return underlined;
            }
            set
            {
                textBoxWithName.TextDecorations.Clear();
                if (value == true)
                {
                    textBoxWithName.TextDecorations.Add(new TextDecoration()
                    {
                        Location = TextDecorationLocation.Underline,
                        //PenThicknessUnit = TextDecorationUnit.Pixel
                    });
                }
            }
        }
        public FontStyle textBoxFontStyle
        {
            get { return textBoxWithName.FontStyle; }
            set
            {
                if (textBoxWithName.FontStyle != value)
                {
                    textBoxWithName.FontStyle = value;
                }
            }
        }
        public FontWeight textBoxFontWeight
        {
            get { return textBoxWithName.FontWeight; }
            set
            {
                if (textBoxWithName.FontWeight != value)
                {
                    textBoxWithName.FontWeight = value;
                }
            }
        }
        public bool isInTextEditingMode
        {
            get { return textBoxWithName.IsKeyboardFocused; }
            set
            {
                if (textBoxWithName.IsKeyboardFocused != value)
                {
                    if (value == true)
                    {
                        this.thumbMove.Visibility = Visibility.Hidden;

                        //Keyboard focus seems to return false when component is not yet attached to parent.
                        //Issue has been fixed in the applyDefaultProperties() method by adding a handler to this.Loaded event
                        textBoxWithName.Focus();
                        textBoxWithName.SelectAll();
                    }
                    else
                    {
                        FocusManager.SetFocusedElement(capsule.hostWindow, capsule.hostWindow);
                    }

                    this.NotifyPropertyChanged("isInTextEditingMode");
                }
            }
        }

        private void initComponents(FrameworkElement moveBounds)
        {
            gridTopContainer = new Grid();
            borderDecorator = new Border() { Margin = new Thickness(RESIZABLE_BORDER_THICKNESS) };
            textBoxWithName = new TextBox();
            base.initSelectionBorder();
            initResizeHandles(moveBounds);
            initRectDecorators();
            thumbMove = new ThumbMoveName(this, moveBounds) { Opacity = 0.0, Margin = new Thickness(RESIZABLE_BORDER_THICKNESS) };
        }
        private void loadComponents()
        {
            //====Layout====
            gridTopContainer.Children.Add(base.borderSelection);

            borderDecorator.Child = textBoxWithName;
            gridTopContainer.Children.Add(borderDecorator);

            base.addRectDecorators(gridTopContainer);
            base.addResizeHandles(gridTopContainer);
            gridTopContainer.Children.Add(thumbMove);

            this.Content = this.gridTopContainer;
            //===============
        }
        private void initResizeHandles(FrameworkElement bounds)
        {
            thumbTop = new ResizeThumbName(this, bounds);
            thumbLeft = new ResizeThumbName(this, bounds);
            thumbRight = new ResizeThumbName(this, bounds);
            thumbBottom = new ResizeThumbName(this, bounds);
            thumbTopLeft = new ResizeThumbName(this, bounds);
            thumbTopRight = new ResizeThumbName(this, bounds);
            thumbBottomLeft = new ResizeThumbName(this, bounds);
            thumbBottomRight = new ResizeThumbName(this, bounds);

            initResizeHandlesProperties();
        }
        private void initCrucialProperties(Point initialPosition)
        {
            this.HorizontalAlignment = HorizontalAlignment.Left;
            this.VerticalAlignment = VerticalAlignment.Top;
            this.MinWidth = 70;
            this.MinHeight = 40;
            this.Margin = new Thickness(initialPosition.X - RESIZABLE_BORDER_THICKNESS, initialPosition.Y - RESIZABLE_BORDER_THICKNESS, 0, 0);
            this.PreviewMouseRightButtonDown += nameModel_PreviewMouseRightButtonDown;

            textBoxWithName.IsTabStop = false;
            textBoxWithName.AcceptsReturn = true;
            textBoxWithName.TextWrapping = TextWrapping.WrapWithOverflow;
            textBoxWithName.IsTabStop = false;
            textBoxWithName.BorderThickness = new Thickness(0); //Removes default border
            textBoxWithName.TextDecorations = new TextDecorationCollection();
            textBoxWithName.KeyDown += TextBoxWithName_KeyDown;
            textBoxWithName.TextChanged += TextBoxWithName_TextChanged;
            textBoxWithName.LostFocus += TextBoxWithName_LostFocus;
            textBoxWithName.Loaded += TextBoxWithName_Loaded;

            //borderThickness = new Thickness(0);
            Binding bindingBorderBrush = new Binding("BorderBrush");
            bindingBorderBrush.Mode = BindingMode.OneWay;
            bindingBorderBrush.Source = this;
            borderDecorator.SetBinding(BorderBrushProperty, bindingBorderBrush);

            Binding bindingBackground = new Binding("Background");
            bindingBackground.Mode = BindingMode.OneWay;
            bindingBackground.Source = this;
            textBoxWithName.SetBinding(BackgroundProperty, bindingBackground);

            Binding bindingForeground = new Binding("Foreground");
            bindingForeground.Mode = BindingMode.OneWay;
            bindingForeground.Source = this;
            textBoxWithName.SetBinding(ForegroundProperty, bindingForeground);
            changeBorderDecoratorEdgeMode(EdgeMode.Aliased);
        }
        private void applyDefaultProperties(ComponentStyle defaultLookSettings)
        {
            if (defaultLookSettings == null)
            {
                return;
            }

            this.borderThickness = defaultLookSettings.nameBorderThickness;
            this.BorderBrush = new SolidColorBrush(defaultLookSettings.nameBorderColor);
            this.Background = new SolidColorBrush(defaultLookSettings.nameBackColor);
            this.Foreground = new SolidColorBrush(defaultLookSettings.nameForeColor);

            this.horizontalTextAlignment = defaultLookSettings.hTextAlignment;
            this.verticalTextAlignment = defaultLookSettings.vTextAlignment;
            textBoxWithName.Text = defaultLookSettings.nameText;
            this.FontSize = defaultLookSettings.nameFontSize;
            this.FontFamily = new FontFamily(defaultLookSettings.nameFontName);

            this.textBoxFontWeight = (defaultLookSettings.nameIsBold) ? FontWeights.Bold : FontWeights.Normal;
            this.textBoxFontStyle = (defaultLookSettings.nameIsItalic) ? FontStyles.Italic : FontStyles.Normal;

            this.isTextUnderlined = (defaultLookSettings.nameIsUnderline) ? true : false;

            if (defaultLookSettings.nameIsEditOnCreation)
            {
                this.Loaded += (sender, e) =>
                {
                    //This fix is relevant only when a new NameComponent is highlighted on creation
                    enterTextEditingMode();
                };
            }
        }

        private void TextBoxWithName_Loaded(object sender, RoutedEventArgs e)
        {
            //This is a quick fix that makes inner component (the border) of the TextBox not SnapToDevicePixels
            //  and prevents the textbox from shifting its position
            ReusableFunctions.RemovePixelSnappingFromEachChild(textBoxWithName);
            borderDecorator.Child = null;
            borderDecorator.Child = textBoxWithName;
        }
        private void TextBoxWithName_LostFocus(object sender, RoutedEventArgs e)
        {
            thumbMove.Visibility = Visibility.Visible; //Make thumb accept input to enable dragging
            this.NotifyPropertyChanged("isInTextEditingMode");
        }
        private void TextBoxWithName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.NotifyPropertyChanged("text");
        }
        private void TextBoxWithName_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Escape && thumbMove.Visibility != Visibility.Visible)
            {
                //textBoxWithName.IsFocused - checks logical focus
                //textBoxWithName.IsKeyboardFocused - checks keyboard focus (keyboard focus will have logical focus as well)
                //If this element received the KeyDown event, it already has a keyboard focus - no need to check

                exitTextEditingMode();
                e.Handled = true;
            }

            if (e.Key == Key.Tab)
            {
                e.Handled = true;
            }
        }
        private void nameModel_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            capsule.hostWindow.selectedCapsule = capsule;
            capsule.hostWindow.requestNameContextMenu(this);
            e.Handled = true;
        }

        public void toggleUnderline()
        {
            isTextUnderlined = !isTextUnderlined;
        }
        public void toggleItalic()
        {
            textBoxWithName.FontStyle = (textBoxWithName.FontStyle == FontStyles.Normal) ? FontStyles.Italic : FontStyles.Normal;
        }
        public void toggleBold()
        {
            textBoxWithName.FontWeight = (textBoxWithName.FontWeight == FontWeights.Normal) ? FontWeights.Bold : FontWeights.Normal;
        }
        public void autoAdjustSize()
        {
            this.Width = double.NaN;
            this.Height = double.NaN;
        }
        public void enterTextEditingMode()
        {
            this.isInTextEditingMode = true;
        }
        public void exitTextEditingMode()
        {
            this.isInTextEditingMode = false;
        }
    }
}
