﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace FloorPlan.Components.Main
{
    public class Capsule : INotifyPropertyChanged
    {
        public MainWindow hostWindow;
        private Grid gridArtBoard;

        private PhotoComponent _photoComponent;
        private NameComponent _nameComponent;
        private bool _isSelected = false;

        public Capsule(MainWindow hostWindow, Grid gridArtBoard, PhotoComponent photoComponent, NameComponent nameComponent)
        {
            this.hostWindow = hostWindow;
            this.gridArtBoard = gridArtBoard;

            this.photoComponent = photoComponent;
            this.nameComponent = nameComponent;

            hostWindow.listCapsules.Add(this);
        }

        public PhotoComponent photoComponent
        {
            get { return _photoComponent; }
            set
            {
                if (_photoComponent != value)
                {
                    PhotoComponent oldPhoto = _photoComponent;
                    _photoComponent = value;
                    PhotoComponent newPhoto = _photoComponent;

                    if (oldPhoto != null)
                    {
                        if (gridArtBoard.Children.Contains(oldPhoto))
                        {
                            gridArtBoard.Children.Remove(oldPhoto);
                        }
                        oldPhoto.Delete();
                    }

                    if (newPhoto != null)
                    {
                        newPhoto.capsule = this;
                    }
                    else if (nameComponent == null)
                    {
                        performCleanup();
                    }

                    this.NotifyPropertyChanged("photoComponent");
                }
            }
        }
        public NameComponent nameComponent
        {
            get { return _nameComponent; }
            set
            {
                if (_nameComponent != value)
                {
                    NameComponent oldName = _nameComponent;
                    _nameComponent = value;
                    NameComponent newName = _nameComponent;

                    if (oldName != null)
                    {
                        if (gridArtBoard.Children.Contains(oldName))
                        {
                            gridArtBoard.Children.Remove(oldName);
                        }
                    }

                    if (newName != null)
                    {
                        nameComponent.capsule = this;
                    }
                    else if (photoComponent == null)
                    {
                        performCleanup();
                    }

                    this.NotifyPropertyChanged("nameComponent");
                }
            }
        }
        public bool isSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    if (photoComponent != null)
                    {
                        photoComponent.isSelected = (_isSelected) ? true : false;
                    }
                    if (nameComponent != null)
                    {
                        nameComponent.isSelected = (_isSelected) ? true : false;
                    }
                }
            }
        }

        public void Delete()
        {
            photoComponent = null;
            nameComponent = null;
        }
        public void DeleteComponent(UIElement component)
        {
            if (component == photoComponent)
            {
                this.photoComponent = null;
            }
            else if (component == nameComponent)
            {
                this.nameComponent = null;
            }
        }
        public void addComponentsToGrid()
        {
            if (nameComponent != null && !gridArtBoard.Children.Contains(nameComponent))
            {
                gridArtBoard.Children.Add(nameComponent);
            }
            if (photoComponent != null && !gridArtBoard.Children.Contains(photoComponent))
            {
                gridArtBoard.Children.Add(photoComponent);
            }
        }
        public void moveGroup(double xChange, double yChange)
        {
            if (photoComponent != null)
            {
                photoComponent.setPosition(photoComponent.calculateNewPosition(xChange, yChange));
            }

            if (nameComponent != null)
            {
                nameComponent.setPosition(nameComponent.calculateNewPosition(xChange, yChange));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        private void performCleanup()
        {
            if (nameComponent == null && photoComponent == null)
            {
                if (hostWindow.selectedCapsule == this)
                {
                    hostWindow.selectedCapsule = null;
                }

                if (hostWindow.listCapsules.Contains(this))
                {
                    hostWindow.listCapsules.Remove(this);
                }
            }
        }
    }
}
