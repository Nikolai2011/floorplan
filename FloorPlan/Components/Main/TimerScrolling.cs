﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Controls;

namespace FloorPlan.Features
{
    public class TimerScrolling : DispatcherTimer
    {
        ScrollViewer controlToScroll;
        Grid gridWithContent;

        private const int timerScrollAmount = 5;
        enum ScrollingDirection { none = 0, left = 1, top = 2, right = 4, bottom = 8 }
        ScrollingDirection scrollDirection = ScrollingDirection.none;

        public TimerScrolling(ScrollViewer controlToScroll, Grid gridWithContent)
        {
            this.controlToScroll = controlToScroll;
            this.gridWithContent = gridWithContent;
            this.Interval = TimeSpan.FromMilliseconds(10);

            this.Tick += scrollingOnSelection_Tick;
        }

        private void scrollingOnSelection_Tick(object sender, EventArgs e)
        {
            if (!scrollIsExpected() || scrollDirection == ScrollingDirection.none)
            {
                this.Stop();
                return;
            }

            //Increase/decrease scrolling speed at different zoom values
            double changeFactorX = ((ScaleTransform)gridWithContent.LayoutTransform).ScaleX;
            double changeFactorY = ((ScaleTransform)gridWithContent.LayoutTransform).ScaleY;
            //=========================================

            double offsetX = controlToScroll.HorizontalOffset;
            double offsetY = controlToScroll.VerticalOffset;

            if ((scrollDirection & ScrollingDirection.left) == ScrollingDirection.left)
            {
                if (offsetX > 0)
                {
                    controlToScroll.ScrollToHorizontalOffset(offsetX - timerScrollAmount * changeFactorX);
                }
            }
            else if ((scrollDirection & ScrollingDirection.right) == ScrollingDirection.right)
            {
                if (offsetX < controlToScroll.ScrollableWidth)
                {
                    controlToScroll.ScrollToHorizontalOffset(offsetX + timerScrollAmount * changeFactorX);
                }
            }

            if ((scrollDirection & ScrollingDirection.top) == ScrollingDirection.top)
            {
                if (offsetY > 0)
                {
                    controlToScroll.ScrollToVerticalOffset(offsetY - timerScrollAmount * changeFactorY);
                }
            }
            else if ((scrollDirection & ScrollingDirection.bottom) == ScrollingDirection.bottom)
            {
                if (offsetY < controlToScroll.ScrollableHeight)
                {
                    controlToScroll.ScrollToVerticalOffset(offsetY + timerScrollAmount * changeFactorY);
                }
            }
        }
        public bool scrollIsExpected()
        {
            if (!gridWithContent.IsMouseOver)
            {
                return false;
            }

            Point mouseToScrollViewer = Mouse.GetPosition(controlToScroll);
            Rect viewPortRect = new Rect(new Point(1, 1), new Size(controlToScroll.ViewportWidth - 2.0, controlToScroll.ViewportHeight - 2.0));
            if (!viewPortRect.Contains(mouseToScrollViewer))
            {
                scrollDirection = ScrollingDirection.none;

                if (mouseToScrollViewer.X < viewPortRect.X)
                {
                    scrollDirection |= ScrollingDirection.left;
                }
                else if (mouseToScrollViewer.X > viewPortRect.Width + viewPortRect.X)
                {
                    scrollDirection |= ScrollingDirection.right;
                }

                if (mouseToScrollViewer.Y < viewPortRect.Y)
                {
                    scrollDirection |= ScrollingDirection.top;
                }
                else if (mouseToScrollViewer.Y > viewPortRect.Height + viewPortRect.Y)
                {
                    scrollDirection |= ScrollingDirection.bottom;
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
