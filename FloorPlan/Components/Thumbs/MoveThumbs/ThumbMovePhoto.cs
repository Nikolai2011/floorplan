﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Input;

using FloorPlan.Model.ThumbModels;
using FloorPlan.Components.Main;

namespace FloorPlan.Components.Thumbs.MoveThumbs
{
    public class ThumbMovePhoto : ThumbMoveModel
    {
        private PhotoComponent photoComponent;

        public ThumbMovePhoto(PhotoComponent parent, FrameworkElement bounds) : base(parent, bounds)
        {
            this.photoComponent = parent;
            DragDelta += MoveThumb_DragDelta;
            DragStarted += MoveThumb_DragStarted;
        }

        private void MoveThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (photoComponent.capsule != null)
            {
                photoComponent.capsule.hostWindow.selectedCapsule = photoComponent.capsule;
            }
        }
        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (photoComponent != null)
            {
                if (photoComponent.capsule != null)
                {
                    photoComponent.capsule.hostWindow.timerScrolling.Start();
                }

                //Calculate new position of this PhotoComponent element
                Point newPosition = calculateNewPosition(e.HorizontalChange, e.VerticalChange);

                if (photoComponent.capsule.hostWindow.isMultipleDraggingEnabled == true && photoComponent.capsule != null)
                {
                    photoComponent.capsule.moveGroup(newPosition.X - photoComponent.Margin.Left, newPosition.Y - photoComponent.Margin.Top);
                    this.Cursor = (photoComponent.capsule.nameComponent == null) ? Cursors.SizeAll : Cursors.ScrollAll;
                }
                else
                {
                    setPosition(newPosition);
                    this.Cursor = Cursors.SizeAll;
                }
            }

            e.Handled = true;
        }
    }
}
