﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Input;

using FloorPlan.Model.ThumbModels;
using FloorPlan.Components.Main;

namespace FloorPlan.Components.Thumbs.MoveThumbs
{
    public class ThumbMoveName : ThumbMoveModel
    {
        private NameComponent nameComponent;

        public ThumbMoveName(NameComponent parent, FrameworkElement bounds) : base(parent, bounds)
        {
            this.nameComponent = parent;
            DragStarted += ThumbMoveName_DragStarted;
            DragDelta += new DragDeltaEventHandler(this.ThumbMove_DragDelta);
            MouseDoubleClick += ThumbMoveName_MouseDoubleClick;
        }

        private void ThumbMoveName_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (nameComponent.capsule != null)
            {
                nameComponent.capsule.hostWindow.selectedCapsule = nameComponent.capsule;
            }
        }
        private void ThumbMoveName_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            nameComponent.enterTextEditingMode();
        }
        private void ThumbMove_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (nameComponent != null)
            {
                if (nameComponent.capsule != null)
                {
                    nameComponent.capsule.hostWindow.timerScrolling.Start();
                }

                //Calculate new position of this NameComponent element
                Point newPosition = calculateNewPosition(e.HorizontalChange, e.VerticalChange);

                if (nameComponent.capsule.hostWindow.isMultipleDraggingEnabled == true && nameComponent.capsule != null)
                {
                    nameComponent.capsule.moveGroup(newPosition.X - nameComponent.Margin.Left, newPosition.Y - nameComponent.Margin.Top);
                    this.Cursor = (nameComponent.capsule.photoComponent == null) ? Cursors.SizeAll : Cursors.ScrollAll;
                }
                else
                {
                    setPosition(newPosition);
                    this.Cursor = Cursors.SizeAll;
                }
            }
            e.Handled = true;
        }
    }
}
