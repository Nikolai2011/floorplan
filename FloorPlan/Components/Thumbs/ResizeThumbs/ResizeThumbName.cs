﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Media;

using FloorPlan.Model.ThumbModels;
using FloorPlan.Components.Main;

namespace FloorPlan.Components.Thumbs.ResizeThumbs
{
    class ResizeThumbName : ThumbResizeModel
    {
        private NameComponent nameModel;
        private FrameworkElement moveBounds;

        public ResizeThumbName(NameComponent parent, FrameworkElement moveBounds) : base(parent, moveBounds)
        {
            this.moveBounds = moveBounds;
            this.nameModel = parent;
            DragDelta += new DragDeltaEventHandler(this.ResizeThumb_DragDelta);
            DragStarted += resizeThumb_DragStarted;
        }

        private void resizeThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (nameModel.capsule != null)
            {
                nameModel.capsule.hostWindow.selectedCapsule = nameModel.capsule;
            }
        }

        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (nameModel != null)
            {
                if (nameModel.capsule != null)
                {
                    nameModel.capsule.hostWindow.timerScrolling.Start();
                }

                applyNewSize(e.HorizontalChange, e.VerticalChange);
            }

            e.Handled = true;
        }
    }
}
