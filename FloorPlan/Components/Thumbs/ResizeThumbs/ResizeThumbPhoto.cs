﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Media;

using FloorPlan.Model.ThumbModels;
using FloorPlan.Components.Main;

namespace FloorPlan.Components.Thumbs.ResizeThumbs
{
    class ResizeThumbPhoto : ThumbResizeModel
    {
        private PhotoComponent photoModel;
        private FrameworkElement moveBounds;

        public ResizeThumbPhoto(PhotoComponent parent, FrameworkElement moveBounds) : base(parent, moveBounds)
        {
            this.moveBounds = moveBounds;
            this.photoModel = parent;
            DragDelta += new DragDeltaEventHandler(this.ResizeThumb_DragDelta);
            DragStarted += resizeThumb_DragStarted;
        }

        private void resizeThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            if (photoModel.capsule != null)
            {
                photoModel.capsule.hostWindow.selectedCapsule = photoModel.capsule;
            }
        }

        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (photoModel != null)
            {
                if (photoModel.capsule != null)
                {
                    photoModel.capsule.hostWindow.timerScrolling.Start();
                }

                applyNewSize(e.HorizontalChange, e.VerticalChange);
            }

            e.Handled = true;
        }
    }
}
