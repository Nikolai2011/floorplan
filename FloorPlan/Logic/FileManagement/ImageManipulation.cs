﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing;

using FloorPlan.Features;

namespace FloorPlan.Logic.FileManagement
{
    static class ImageManipulation
    {
        public static BitmapSource extractImageFromFile(string filePath, bool reportExceptions)
        {
            if (!File.Exists(filePath))
            {
                if (reportExceptions)
                {
                    MessageBox.Show("File does not exist.\n\nPath: " + filePath, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                return null;
            }

            try
            {
                using (Stream streamFromFile = File.Open(filePath, FileMode.Open))
                {
                    using (Image image = Image.FromStream(streamFromFile))
                    {
                        if (image.GetFrameCount(new System.Drawing.Imaging.FrameDimension(image.FrameDimensionsList[0])) != 1)
                        {
                            if (reportExceptions)
                            {
                                MessageBox.Show("Animated images are not supported.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                            }
                            return null;
                        }
                    }
                }

                using (Stream streamFromFile = File.Open(filePath, FileMode.Open))
                {
                    //========STREAM loading==========
                    BitmapImage floorMapImage = new BitmapImage();
                    floorMapImage.BeginInit();
                    floorMapImage.StreamSource = streamFromFile;
                    floorMapImage.CacheOption = BitmapCacheOption.OnLoad;
                    floorMapImage.EndInit();

                    //===========Transform image into 96 dpi===========
                    //from https://social.msdn.microsoft.com/Forums/vstudio/en-US/35db45e3-ebd6-4981-be57-2efd623ea439/wpf-bitmapsource-dpi-change?forum=wpf
                    //double dpi = 96;
                    //int width = floorMapImage.PixelWidth;
                    //int height = floorMapImage.PixelHeight;

                    //int stride = width * 4; // 4 bytes per pixel
                    //byte[] pixelData = new byte[stride * height];
                    //floorMapImage.CopyPixels(pixelData, stride, 0);

                    //BitmapSource bmpSource = BitmapSource.Create(width, height, dpi, dpi, PixelFormats.Bgra32, null, pixelData, stride);
                    //==================================================

                    return floorMapImage;
                }
            }
            catch (Exception ex)
            {
                if (reportExceptions)
                {
                    MessageBox.Show("Could not create image from file. \nPath: " + filePath + "\nError: "+ex.Message, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }

            return null;
        }
        public static string appendFileExtension(string fileName, BitmapEncoder encoder)
        {
            if (encoder is JpegBitmapEncoder)
            {
                ((JpegBitmapEncoder)encoder).QualityLevel = 100;
                if ((!fileName.EndsWith(".jpg")) && (!fileName.EndsWith(".jpeg")))
                {
                    fileName = fileName + ".jpg";
                }
            }
            else if (encoder is BmpBitmapEncoder)
            {
                if (!fileName.EndsWith(".bmp"))
                {
                    fileName = fileName + ".bmp";
                }
            }
            else if (encoder is PngBitmapEncoder)
            {
                if (!fileName.EndsWith(".png"))
                {
                    fileName = fileName + ".png";
                }
            }
            else if (encoder is WmpBitmapEncoder)
            {
                if (!fileName.EndsWith(".wdp"))
                {
                    fileName = fileName + ".wdp";
                }
            }
            else if (encoder is GifBitmapEncoder)
            {
                if (!fileName.EndsWith(".gif"))
                {
                    fileName = fileName + ".gif";
                }
            }
            else if (encoder is TiffBitmapEncoder)
            {
                if (!fileName.EndsWith(".tiff"))
                {
                    fileName = fileName + ".tiff";
                }
            }
            return fileName;
        }
        public static bool exportImageToFile(string pathToFile, string fileName, BitmapEncoder encoder)
        {
            try
            {
                if (File.Exists(pathToFile + "\\" + fileName))
                {
                    MessageBoxResult result = MessageBox.Show("A file with the same name exists in the specified path.\n\nDo you want to replace it with a new file?", "File Replacement", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.No)
                    {
                        return false;
                    }
                }
                else if (Directory.Exists(pathToFile + "\\" + fileName))
                {
                    MessageBox.Show("\"" + fileName + "\"" + " is a directory in the following path: \"" + pathToFile + "\"\n\nTry using a different file name.", "Invalid File Path", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }

                using (Stream streamImageSaver = File.Create(pathToFile + "\\" + fileName))
                {
                    encoder.Save(streamImageSaver);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
            return false;
        }
    }
}
