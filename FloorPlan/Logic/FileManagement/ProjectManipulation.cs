﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

using FloorPlan.Logic.Serialization.Data;
using FloorPlan.Interaction.CustomDialogBoxes;
using FloorPlan.Logic.Serialization.Functions;
using FloorPlan.Features;
using FloorPlan.Components.Main;
using FloorPlan.Model.Main;

namespace FloorPlan.Logic.FileManagement
{
    public static class ProjectManipulation
    {
        public const string COMMON_SETTINGS_FILE_EXTENSION = ".cst";
        public const string STYLES_FILE = "Styles" + COMMON_SETTINGS_FILE_EXTENSION;
        public const string CREATION_SETTINGS_FILE = "CreationSettings" + COMMON_SETTINGS_FILE_EXTENSION;
        public const string FOLDER_WITH_COMMON_SETTINGS = "CommonSettings";

        public const string FOLDER_WITH_TEMPORARY_PROJECT = "TempProject";

        public const string FOLDER_WITH_PROJECTS = "Projects";
        public const string FOLDER_WITH_IMAGES = "Images";
        public const string FOLDER_WITH_PHOTOS = "Photos";
        public const string PROJECT_FILE_EXTENSION = ".flpr";

        public static CommonSettings loadCommonSettings()
        {
            CommonSettings commonSettings = null;

            string creationSettingsFilePath = FOLDER_WITH_COMMON_SETTINGS + "\\" + CREATION_SETTINGS_FILE;

            if (File.Exists(creationSettingsFilePath))
            {
                commonSettings = new CommonSettings();
                commonSettings.componentCreationSettings = DataTransfers.readFromJsonFile<ComponentStyle>(creationSettingsFilePath);

                List<ComponentStyle> styles = loadStyles();

                if (styles != null)
                {
                    commonSettings.styles = styles;
                }

                return commonSettings;
            }
            else
            {
                return null;
            }
        }

        private static List<ComponentStyle> loadStyles()
        {
            string stylesFilePath = FOLDER_WITH_COMMON_SETTINGS + "\\" + STYLES_FILE;
            if (File.Exists(stylesFilePath))
            {
                List<ComponentStyle> styles = DataTransfers.readFromJsonFile<List<ComponentStyle>>(stylesFilePath);
                return styles;
            }
            else
            {
                return null;
            }
        }

        public static bool saveCommonSettings(CommonSettings commonSettings)
        {
            bool wasSaved = false;
            try
            {
                if (commonSettings == null)
                {
                    return false;
                }
                else if (createDirectory(FOLDER_WITH_COMMON_SETTINGS) == false)
                {
                    return false;
                }

                DataTransfers.writeToJsonFile(FOLDER_WITH_COMMON_SETTINGS + "\\" + CREATION_SETTINGS_FILE, commonSettings.componentCreationSettings);
                DataTransfers.writeToJsonFile(FOLDER_WITH_COMMON_SETTINGS + "\\" + STYLES_FILE, commonSettings.styles);
                wasSaved = true;
            }
            catch
            {
                wasSaved = false;
            }
            return wasSaved;
        }

        public static bool createNewProject(MainWindow projectOwner)
        {
            string originalProject = null;
            string tempProject = null;
            bool wasCreated = false;

            if (createDirectory(FOLDER_WITH_PROJECTS) == false || createDirectory(FOLDER_WITH_TEMPORARY_PROJECT) == false)
            {
                return false;
            }

            try
            {
                DlgProjectCreation projectCreationDlg = new DlgProjectCreation();
                projectCreationDlg.Owner = projectOwner;
                if (projectCreationDlg.ShowDialog() == true)
                {
                    originalProject = projectCreationDlg.directoryPathOfNewProject;
                    tempProject = createTemporaryProject(originalProject);

                    if (tempProject != null)
                    {
                        projectOwner.clearWindow();
                        projectOwner.currentProjectPath = tempProject;
                        projectOwner.originalProjectPath = originalProject;
                        saveProject(projectOwner, originalProject, Path.GetFileName(originalProject));
                        wasCreated = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
                wasCreated = false;
            }
            return wasCreated;
        }
        public static bool saveProject(MainWindow projectOwner, string projectPath, string projectName)
        {
            bool wasSaved = false;
            try
            {
                if (projectPath == null || createProjectTree(projectPath) == false)
                {
                    return false;
                }

                ProjectData projectData = packProjectData(projectOwner);
                if (projectData != null)
                {
                    DataTransfers.writeToJsonFile(projectPath + "\\" + projectName + PROJECT_FILE_EXTENSION, projectData);
                    projectOwner.selectedCapsule = null;
                    wasSaved = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save the project: \n\n" + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return wasSaved;
        }
        public static bool openProject(MainWindow projectOwner, Window dialogOwner)
        {
            bool wasLoaded = false;
            try
            {
                if (createDirectory(FOLDER_WITH_PROJECTS) == false || createDirectory(FOLDER_WITH_TEMPORARY_PROJECT) == false)
                {
                    return false;
                }
                
                DirectoryInfo dirProjects = new DirectoryInfo(FOLDER_WITH_PROJECTS);
                Microsoft.Win32.OpenFileDialog openFileDlgProjects = ReusableDialogBoxes.openFileDlgProjects;
                openFileDlgProjects.InitialDirectory = dirProjects.FullName;
                string savedWorkingDir = Environment.CurrentDirectory;

                if (openFileDlgProjects.ShowDialog(dialogOwner) == true)
                {
                    ProjectData projectData = DataTransfers.readFromJsonFile<ProjectData>(openFileDlgProjects.FileName);
                    if (projectData != null)
                    {
                        if (projectData.originalProjectPath == null)
                        {
                            MessageBox.Show("Unable to load the project: \n\n\"originalProjectPath\" is missing!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            string tempProject = createTemporaryProject(projectData.originalProjectPath);
                            if (tempProject != null)
                            {
                                projectOwner.clearWindow();
                                projectOwner.currentProjectPath = tempProject;
                                projectOwner.originalProjectPath = projectData.originalProjectPath;
                                unpackProjectData(projectOwner, projectData);
                                wasLoaded = true;
                            }
                        }
                    }
                }
                if (Directory.Exists(savedWorkingDir))
                {
                    Environment.CurrentDirectory = savedWorkingDir;
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
            return wasLoaded;
        }
        public static string createTemporaryProject(string originalProjectPath)
        {
            string tempProjectPath = null;
            ReusableFunctions.deleteFolder(FOLDER_WITH_TEMPORARY_PROJECT);
            if (!Directory.Exists(originalProjectPath) || createDirectory(FOLDER_WITH_TEMPORARY_PROJECT) == false)
            {
                return null;
            }

            try
            {
                ReusableFunctions.directoryCopy(originalProjectPath, FOLDER_WITH_TEMPORARY_PROJECT, true);
                tempProjectPath = FOLDER_WITH_TEMPORARY_PROJECT;
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
                return null;
            }

            return tempProjectPath;
        }

        public static bool createProjectTree(string projectFolderPath)
        {
            if (projectFolderPath == null)
            {
                return false;
            }

            try
            {
                Directory.CreateDirectory(projectFolderPath);
                Directory.CreateDirectory(projectFolderPath + "\\" + FOLDER_WITH_IMAGES);
                Directory.CreateDirectory(projectFolderPath + "\\" + FOLDER_WITH_PHOTOS);
                return true;
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
                return false;
            }
        }
        public static bool createDirectory(string dirName)
        {
            try
            {
                if (Directory.Exists(dirName))
                {
                    return true;
                }
                else
                {
                    Directory.CreateDirectory(dirName);
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not locate and create the directory \"" + dirName + "\"\n\nError: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        private static ProjectData packProjectData(MainWindow projectOwner)
        {
            ProjectData projectData = new ProjectData();
            projectData.originalProjectPath = projectOwner.originalProjectPath;
            projectData.floorMapFileName = projectOwner.floorMapFileName;
            projectData.gridChildrenCount = projectOwner.gridArtBoard.Children.Count;

            foreach (Capsule capsule in projectOwner.listCapsules)
            {
                CapsuleData capsuleData = new CapsuleData();

                if (capsule.photoComponent != null)
                {
                    PhotoComponentData photoComponentData = new PhotoComponentData();
                    photoComponentData.margin = new Thickness(capsule.photoComponent.Margin.Left + DynamicModel.RESIZABLE_BORDER_THICKNESS, capsule.photoComponent.Margin.Top + DynamicModel.RESIZABLE_BORDER_THICKNESS, 0, 0);
                    photoComponentData.size = new Size(capsule.photoComponent.Width - 2 * DynamicModel.RESIZABLE_BORDER_THICKNESS, capsule.photoComponent.Height - 2 * DynamicModel.RESIZABLE_BORDER_THICKNESS);
                    photoComponentData.orderIndex = projectOwner.gridArtBoard.Children.IndexOf(capsule.photoComponent);
                    photoComponentData.photoFileName = capsule.photoComponent.photoFileName;
                    photoComponentData.borderThickness = capsule.photoComponent.BorderThickness;

                    SolidColorBrush brush;
                    if ((brush = capsule.photoComponent.BorderBrush as SolidColorBrush) != null)
                    {
                        photoComponentData.borderColor = brush.Color;
                    }

                    capsuleData.photoComponentData = photoComponentData;
                }


                if (capsule.nameComponent != null)
                {
                    NameComponentData nameComponentData = new NameComponentData();
                    nameComponentData.margin = new Thickness(capsule.nameComponent.Margin.Left + DynamicModel.RESIZABLE_BORDER_THICKNESS, capsule.nameComponent.Margin.Top + DynamicModel.RESIZABLE_BORDER_THICKNESS, 0, 0);
                    nameComponentData.size = new Size(capsule.nameComponent.Width, capsule.nameComponent.Height);
                    nameComponentData.orderIndex = projectOwner.gridArtBoard.Children.IndexOf(capsule.nameComponent);
                    nameComponentData.borderThickness = capsule.nameComponent.borderThickness;

                    SolidColorBrush brush;
                    if ((brush = capsule.nameComponent.BorderBrush as SolidColorBrush) != null)
                    {
                        nameComponentData.borderColor = brush.Color;
                    }

                    if ((brush = capsule.nameComponent.Background as SolidColorBrush) != null)
                    {
                        nameComponentData.backgroundColor = brush.Color;
                    }

                    if ((brush = capsule.nameComponent.Foreground as SolidColorBrush) != null)
                    {
                        nameComponentData.foregroundColor = brush.Color;
                    }

                    nameComponentData.text = capsule.nameComponent.text;
                    nameComponentData.fontSize = capsule.nameComponent.FontSize;
                    nameComponentData.hTextAlignment = capsule.nameComponent.horizontalTextAlignment;
                    nameComponentData.vTextAlignment = capsule.nameComponent.verticalTextAlignment;
                    nameComponentData.fontFamily = capsule.nameComponent.FontFamily.Source;
                    nameComponentData.isBold = (capsule.nameComponent.textBoxFontWeight == FontWeights.Bold) ? true : false;
                    nameComponentData.isItalic = (capsule.nameComponent.textBoxFontStyle == FontStyles.Italic) ? true : false;
                    nameComponentData.isUnderlined = (capsule.nameComponent.isTextUnderlined) ? true : false;

                    capsuleData.nameComponentData = nameComponentData;
                }

                projectData.listCapsules.Add(capsuleData);
            }

            return projectData;
        }
        private static void unpackProjectData(MainWindow projectOwner, ProjectData projectData)
        {
            projectOwner.floorMapFileName = projectData.floorMapFileName;
            string pathToImages = projectData.originalProjectPath + "\\" + FOLDER_WITH_IMAGES + "\\";
            string pathToPhotos = projectData.originalProjectPath + "\\" + FOLDER_WITH_PHOTOS + "\\";

            int countImagesNotLoaded = 0;
            BitmapSource bmpSource;
            if (projectData.floorMapFileName != null)
            {
                bmpSource = ImageManipulation.extractImageFromFile(pathToImages + projectData.floorMapFileName, false);
                if (bmpSource != null)
                {
                    projectOwner.imageFloorMap.Source = bmpSource;
                }
                else
                {
                    countImagesNotLoaded++;
                }
            }


            DynamicModel[] dynamicComponents = new DynamicModel[projectData.gridChildrenCount];
            foreach (CapsuleData capsuleData in projectData.listCapsules)
            {
                PhotoComponent photoComponent = null;
                if (capsuleData.photoComponentData != null)
                {
                    PhotoComponentData photoData = capsuleData.photoComponentData;
                    photoComponent = new PhotoComponent(projectOwner.imageFloorMap, photoData.size, new Point(photoData.margin.Left, photoData.margin.Top));
                    photoComponent.BorderThickness = photoData.borderThickness;
                    photoComponent.BorderBrush = new SolidColorBrush(photoData.borderColor);

                    bmpSource = ImageManipulation.extractImageFromFile(pathToPhotos + photoData.photoFileName, false);
                    if (bmpSource != null)
                    {
                        photoComponent.setPathToPhoto(pathToPhotos + photoData.photoFileName, bmpSource);
                    }
                    else
                    {
                        photoComponent.borderDecoratorBackground = Brushes.Black;
                        countImagesNotLoaded++;
                    }
                    if (photoData.orderIndex >= 0)
                    {
                        dynamicComponents[photoData.orderIndex] = photoComponent;
                    }
                }

                NameComponent nameComponent = null;
                if (capsuleData.nameComponentData != null)
                {
                    NameComponentData nameData = capsuleData.nameComponentData;
                    nameComponent = new NameComponent(projectOwner.imageFloorMap, new Point(nameData.margin.Left, nameData.margin.Top));
                    nameComponent.Width = nameData.size.Width;
                    nameComponent.Height = nameData.size.Height;
                    nameComponent.borderThickness = nameData.borderThickness;
                    nameComponent.BorderBrush = new SolidColorBrush(nameData.borderColor);
                    nameComponent.Background = new SolidColorBrush(nameData.backgroundColor);
                    nameComponent.Foreground = new SolidColorBrush(nameData.foregroundColor);
                    nameComponent.text = nameData.text;
                    nameComponent.FontSize = nameData.fontSize;
                    nameComponent.horizontalTextAlignment = nameData.hTextAlignment;
                    nameComponent.verticalTextAlignment = nameData.vTextAlignment;
                    nameComponent.FontFamily = new FontFamily(nameData.fontFamily);
                    nameComponent.textBoxFontWeight = (nameData.isBold) ? FontWeights.Bold : FontWeights.Normal;
                    nameComponent.textBoxFontStyle = (nameData.isItalic) ? FontStyles.Italic : FontStyles.Normal;
                    nameComponent.isTextUnderlined = (nameData.isUnderlined) ? true : false;
                    if (nameData.orderIndex >= 0)
                    {
                        dynamicComponents[nameData.orderIndex] = nameComponent;
                    }
                }

                Capsule capsule = new Capsule(projectOwner, projectOwner.gridArtBoard, photoComponent, nameComponent);
            }

            for (int i = 0; i < dynamicComponents.Length; i++)
            {
                if (dynamicComponents[i] != null)
                {
                    projectOwner.gridArtBoard.Children.Add(dynamicComponents[i]);
                }
            }

            if (countImagesNotLoaded > 0)
            {
                MessageBox.Show("The number of images not loaded: " + countImagesNotLoaded + ".", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
