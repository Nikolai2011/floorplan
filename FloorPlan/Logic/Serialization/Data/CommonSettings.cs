﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloorPlan.Logic.Serialization.Data
{
    public class CommonSettings
    {
        public ComponentStyle componentCreationSettings = null;
        public List<ComponentStyle> styles = new List<ComponentStyle>();

        public CommonSettings clone()
        {
            CommonSettings obj = new CommonSettings();

            if (this.componentCreationSettings != null)
            {
                obj.componentCreationSettings = this.componentCreationSettings.clone();
            }

            foreach (ComponentStyle style in this.styles)
            {
                obj.styles.Add(style.clone());
            }

            return obj;
        }
    }
}
