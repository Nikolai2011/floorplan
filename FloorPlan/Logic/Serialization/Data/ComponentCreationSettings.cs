﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Media;

namespace FloorPlan.Logic.Serialization.Data
{
    public class ComponentStyle
    {
        public Thickness photoBorderThickness = new Thickness(0);
        public Color photoBorderColor = Colors.Black;
        public Size photoInitialSize = new Size(80.0, 80.0);
        
        public Thickness nameBorderThickness = new Thickness(0);
        public Color nameBorderColor = Colors.Black;
        public Color nameBackColor = Colors.White;
        public Color nameForeColor = Colors.Black;
        public string nameText = "Name\nExample";
        public HorizontalAlignment hTextAlignment = HorizontalAlignment.Center;
        public VerticalAlignment vTextAlignment = VerticalAlignment.Center;
        public bool nameIsUnderline = false;
        public bool nameIsItalic = false;
        public bool nameIsBold = false;
        public bool nameIsEditOnCreation = false;
        public double nameFontSize = 16.0;
        public string nameFontName = "Segoe UI";

        public ComponentStyle clone()
        {
            ComponentStyle obj = base.MemberwiseClone() as ComponentStyle;
            return obj;
        }
    }
}
