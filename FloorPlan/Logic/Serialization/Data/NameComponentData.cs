﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace FloorPlan.Logic.Serialization.Data
{
    public class NameComponentData
    {
        public Thickness margin;
        public Size size;
        public int orderIndex;
        public Thickness borderThickness;
        public Color borderColor;
        public Color backgroundColor;
        public Color foregroundColor;
        public string text;
        public double fontSize;
        public HorizontalAlignment hTextAlignment;
        public VerticalAlignment vTextAlignment;
        public string fontFamily;
        public bool isBold;
        public bool isItalic;
        public bool isUnderlined;
    }
}
