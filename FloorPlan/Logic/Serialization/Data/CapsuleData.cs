﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloorPlan.Logic.Serialization.Data
{
    public class CapsuleData
    {
        public PhotoComponentData photoComponentData;
        public NameComponentData nameComponentData;
    }
}
