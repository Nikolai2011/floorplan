﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace FloorPlan.Logic.Serialization.Data
{
    public class PhotoComponentData
    {
        public Thickness margin;
        public Size size;
        public int orderIndex;
        public string photoFileName;
        public Thickness borderThickness;
        public Color borderColor;
    }
}
