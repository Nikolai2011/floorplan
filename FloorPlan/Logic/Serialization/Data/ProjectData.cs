﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloorPlan.Logic.Serialization.Data
{
    public class ProjectData
    {
        public string originalProjectPath = null;

        public string floorMapFileName = null;

        public int gridChildrenCount;
        public List<CapsuleData> listCapsules = new List<CapsuleData>(30);
    }
}
