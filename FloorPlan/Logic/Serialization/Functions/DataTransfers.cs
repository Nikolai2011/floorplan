﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Web.Script.Serialization;
using System.IO;

namespace FloorPlan.Logic.Serialization.Functions
{
    public class DataTransfers
    {
        public static void writeToJsonFile<T>(string filePath, T objectToWrite)
        {
            if (objectToWrite == null) { return; }

            TextWriter writer = null;
            try
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                writer = new StreamWriter(filePath, false);
                writer.Write(jsonSerializer.Serialize(objectToWrite));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save the project: \n\n" + ex.Message, "Failed To Save", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }

        public static T readFromJsonFile<T>(string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) { return default(T); }

            TextReader reader = null;
            try
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                reader = new StreamReader(filePath);
                return (T)jsonSerializer.Deserialize(reader.ReadToEnd(), typeof(T));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to load the project: \n\n" + ex.Message, "Failed To Load", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
            return default(T);
        }
    }
}
