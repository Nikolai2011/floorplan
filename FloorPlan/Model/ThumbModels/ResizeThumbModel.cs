﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Media;

using FloorPlan.Model.Main;

namespace FloorPlan.Model.ThumbModels
{
    public abstract class ThumbResizeModel : Thumb
    {
        private DynamicModel sizeBoundParent;
        private FrameworkElement moveBounds;

        public ThumbResizeModel(DynamicModel sizeBoundParent, FrameworkElement moveBounds)
        {
            this.moveBounds = moveBounds;
            this.sizeBoundParent = sizeBoundParent;
        }

        protected virtual void applyNewSize(double hChange, double vChange)
        {
            double offset = DynamicModel.RESIZABLE_BORDER_THICKNESS;

            Rect rectBounds = new Rect(moveBounds.RenderSize);
            Rect rectCurrent = new Rect(sizeBoundParent.Margin.Left + offset,
                                        sizeBoundParent.Margin.Top + offset,
                                        sizeBoundParent.RenderSize.Width - 2 * offset,
                                        sizeBoundParent.RenderSize.Height - 2 * offset);
            Rect rectNew = rectCurrent;

            double deltaVertical = vChange, deltaHorizontal = hChange;
            switch (VerticalAlignment)
            {
                case VerticalAlignment.Bottom:
                    deltaVertical = Math.Min(-vChange, sizeBoundParent.ActualHeight - sizeBoundParent.MinHeight);
                    rectNew.Height -= deltaVertical;
                    break;
                case VerticalAlignment.Top:
                    deltaVertical = Math.Min(vChange, sizeBoundParent.ActualHeight - sizeBoundParent.MinHeight);
                    rectNew.Y += deltaVertical;
                    rectNew.Height -= deltaVertical;
                    break;
                default:
                    break;
            }
            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    deltaHorizontal = Math.Min(hChange, sizeBoundParent.ActualWidth - sizeBoundParent.MinWidth);
                    rectNew.X += deltaHorizontal;
                    rectNew.Width -= deltaHorizontal;
                    break;
                case HorizontalAlignment.Right:
                    deltaHorizontal = Math.Min(-hChange, sizeBoundParent.ActualWidth - sizeBoundParent.MinWidth);
                    rectNew.Width -= deltaHorizontal;
                    break;
                default:
                    break;
            }

            if (rectBounds.Contains(rectCurrent))
            {
                if (rectNew.Left < 0)
                {
                    rectNew.X = 0;
                    rectNew.Width = rectCurrent.Right;
                }
                else if (rectNew.Right > rectBounds.Right)
                {
                    rectNew.Width = rectBounds.Right - rectNew.Left;
                }

                if (rectNew.Top < 0)
                {
                    rectNew.Y = 0;
                    rectNew.Height = rectCurrent.Bottom;
                }
                else if (rectNew.Bottom > rectBounds.Bottom)
                {
                    rectNew.Height = rectBounds.Bottom - rectNew.Top;
                }
            }

            sizeBoundParent.Margin = new Thickness(rectNew.Left - offset, rectNew.Top - offset, 0, 0);
            sizeBoundParent.Width = rectNew.Width + 2 * offset;
            sizeBoundParent.Height = rectNew.Height + 2 * offset;
        }
    }
}
