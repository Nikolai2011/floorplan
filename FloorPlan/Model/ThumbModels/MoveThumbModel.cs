﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Input;

using FloorPlan.Model.Main;

namespace FloorPlan.Model.ThumbModels
{
    public abstract class ThumbMoveModel : Thumb
    {
        private DynamicModel positionBoundParent;
        private FrameworkElement moveBounds;

        protected ThumbMoveModel(DynamicModel parent, FrameworkElement bounds)
        {
            this.moveBounds = bounds;
            this.positionBoundParent = parent;
            DragCompleted += MoveThumbModel_DragCompleted;
        }

        protected virtual void MoveThumbModel_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        public void setPosition(Point newPosition)
        {
            positionBoundParent.Margin = new Thickness(newPosition.X, newPosition.Y, 0, 0);
        }

        public Point calculateNewPosition(double xChange, double yChange)
        {
            double offset = DynamicModel.RESIZABLE_BORDER_THICKNESS;

            Rect rectBounds = new Rect(moveBounds.RenderSize);
            Rect rectCurrent = new Rect(positionBoundParent.Margin.Left + offset,
                        positionBoundParent.Margin.Top + offset,
                        positionBoundParent.RenderSize.Width - 2 * offset,
                        positionBoundParent.RenderSize.Height - 2 * offset);
            Rect rectNew = rectCurrent;
            rectNew.X += xChange;
            rectNew.Y += yChange;

            if (rectBounds.Contains(rectCurrent))
            {
                if (rectNew.Left < 0)
                {
                    rectNew.X = 0;
                }
                else if (rectNew.Right > rectBounds.Right)
                {
                    rectNew.X = rectBounds.Right - rectNew.Width;
                }

                if (rectNew.Top < 0)
                {
                    rectNew.Y = 0;
                }
                else if (rectNew.Bottom > rectBounds.Bottom)
                {
                    rectNew.Y = rectBounds.Bottom - rectNew.Height;
                }
            }
            else
            {

            }

            //positionBoundParent.Margin = new Thickness(rectNew.Left - offset, rectNew.Top - offset, 0, 0);

            return new Point(rectNew.Left - offset, rectNew.Y - offset); //New position of the top left corner
        }
    }
}
