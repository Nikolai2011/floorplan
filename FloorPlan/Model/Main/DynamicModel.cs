﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows.Media.Effects;

using FloorPlan.Model.ThumbModels;

namespace FloorPlan.Model.Main
{
    public abstract class DynamicModel : ContentControl
    {
        public const double VISIBLE_BORDER_THICKNESS = 2;
        public const double INNER_INDENT = 0;
        public const double OUTER_INDENT = 3;

        public const double RESIZABLE_BORDER_THICKNESS = VISIBLE_BORDER_THICKNESS + INNER_INDENT + OUTER_INDENT;

        protected Grid gridTopContainer;

        protected Border borderDecorator;
        protected Rectangle borderSelection;
        protected ThumbMoveModel thumbMove;
        protected ThumbResizeModel thumbTop;
        protected ThumbResizeModel thumbLeft;
        protected ThumbResizeModel thumbRight;
        protected ThumbResizeModel thumbBottom;
        protected ThumbResizeModel thumbTopLeft;
        protected ThumbResizeModel thumbTopRight;
        protected ThumbResizeModel thumbBottomLeft;
        protected ThumbResizeModel thumbBottomRight;
        protected Ellipse rectTop = new Ellipse();
        protected Ellipse rectLeft = new Ellipse();
        protected Ellipse rectRight = new Ellipse();
        protected Ellipse rectBottom = new Ellipse();
        protected Ellipse rectTopLeft = new Ellipse();
        protected Ellipse rectTopRight = new Ellipse();
        protected Ellipse rectBottomLeft = new Ellipse();
        protected Ellipse rectBottomRight = new Ellipse();

        protected bool _isSelected = false;
        public bool isSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    if (_isSelected == true)
                    {
                        setDecoratorOpacity(1.0d);
                        borderSelection.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        setDecoratorOpacity(0.0d);
                        borderSelection.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        public DynamicModel()
        {
            this.IsTabStop = false;
        }

        public void initSelectionBorder()
        {
            borderSelection = new Rectangle() { Visibility = Visibility.Hidden };
            borderSelection.StrokeThickness = VISIBLE_BORDER_THICKNESS;
            borderSelection.SnapsToDevicePixels = true;

            //borderSelection.Stroke = Brushes.Blue;
            //borderSelection.StrokeDashArray = DoubleCollection.Parse("4 4");

            double size = 1; //Is this a relative size?
            DrawingBrush brush = new DrawingBrush() { TileMode = TileMode.Tile, ViewportUnits = BrushMappingMode.Absolute };
            GeometryGroup gGroup1 = new GeometryGroup();
            gGroup1.Children.Add(new RectangleGeometry(new Rect(0, 0, size, size)));
            gGroup1.Children.Add(new RectangleGeometry(new Rect(size, size, size, size)));
            GeometryDrawing gDrawing1 = new GeometryDrawing(Brushes.Blue, null, gGroup1);
            GeometryGroup gGroup2 = new GeometryGroup();
            gGroup2.Children.Add(new RectangleGeometry(new Rect(size, 0, size, size)));
            gGroup2.Children.Add(new RectangleGeometry(new Rect(0, size, size, size)));
            GeometryDrawing gDrawing2 = new GeometryDrawing(Brushes.Transparent, null, gGroup2);
            DrawingGroup dGroup = new DrawingGroup();
            dGroup.Children.Add(gDrawing1);
            dGroup.Children.Add(gDrawing2);
            brush.Drawing = dGroup;
            brush.Viewport = new Rect(0, 0, VISIBLE_BORDER_THICKNESS, VISIBLE_BORDER_THICKNESS);
            borderSelection.Stroke = brush;


            borderSelection.Margin = new Thickness(RESIZABLE_BORDER_THICKNESS - VISIBLE_BORDER_THICKNESS - INNER_INDENT);
        }
        protected virtual void addResizeHandles(Panel grid)
        {
            grid.Children.Add(thumbTop);
            grid.Children.Add(thumbLeft);
            grid.Children.Add(thumbRight);
            grid.Children.Add(thumbBottom);
            grid.Children.Add(thumbTopLeft);
            grid.Children.Add(thumbTopRight);
            grid.Children.Add(thumbBottomLeft);
            grid.Children.Add(thumbBottomRight);
        }
        protected virtual void initResizeHandlesProperties()
        {
            SolidColorBrush backgroundBrush = null;
            double cornerSizeIncreaseFactor = 2.0;
            double sideSizeIncreaseFactor = 1.0;
            double opacity = 0.0;

            thumbTop.Cursor = Cursors.SizeNS;
            thumbTop.VerticalAlignment = VerticalAlignment.Top;
            thumbTop.HorizontalAlignment = HorizontalAlignment.Stretch;
            //thumbTop.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbTop.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbTop.Opacity = opacity;
            thumbTop.Background = backgroundBrush;

            thumbLeft.Cursor = Cursors.SizeWE;
            thumbLeft.VerticalAlignment = VerticalAlignment.Stretch;
            thumbLeft.HorizontalAlignment = HorizontalAlignment.Left;
            thumbLeft.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            //thumbLeft.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbLeft.Opacity = opacity;
            thumbLeft.Background = backgroundBrush;

            thumbRight.Cursor = Cursors.SizeWE;
            thumbRight.VerticalAlignment = VerticalAlignment.Stretch;
            thumbRight.HorizontalAlignment = HorizontalAlignment.Right;
            thumbRight.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            //thumbRight.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbRight.Opacity = opacity;
            thumbRight.Background = backgroundBrush;

            thumbBottom.Cursor = Cursors.SizeNS;
            thumbBottom.VerticalAlignment = VerticalAlignment.Bottom;
            thumbBottom.HorizontalAlignment = HorizontalAlignment.Stretch;
            //thumbBottom.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbBottom.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            thumbBottom.Opacity = opacity;
            thumbBottom.Background = backgroundBrush;

            thumbTopLeft.Cursor = Cursors.SizeNWSE;
            thumbTopLeft.VerticalAlignment = VerticalAlignment.Top;
            thumbTopLeft.HorizontalAlignment = HorizontalAlignment.Left;
            thumbTopLeft.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbTopLeft.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbTopLeft.Opacity = opacity;
            thumbTopLeft.Background = backgroundBrush;

            thumbTopRight.Cursor = Cursors.SizeNESW;
            thumbTopRight.VerticalAlignment = VerticalAlignment.Top;
            thumbTopRight.HorizontalAlignment = HorizontalAlignment.Right;
            thumbTopRight.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbTopRight.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbTopRight.Opacity = opacity;
            thumbTopRight.Background = backgroundBrush;

            thumbBottomLeft.Cursor = Cursors.SizeNESW;
            thumbBottomLeft.VerticalAlignment = VerticalAlignment.Bottom;
            thumbBottomLeft.HorizontalAlignment = HorizontalAlignment.Left;
            thumbBottomLeft.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbBottomLeft.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbBottomLeft.Opacity = opacity;
            thumbBottomLeft.Background = backgroundBrush;

            thumbBottomRight.Cursor = Cursors.SizeNWSE;
            thumbBottomRight.VerticalAlignment = VerticalAlignment.Bottom;
            thumbBottomRight.HorizontalAlignment = HorizontalAlignment.Right;
            thumbBottomRight.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbBottomRight.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            thumbBottomRight.Opacity = opacity;
            thumbBottomRight.Background = backgroundBrush;
        }
        protected virtual void addRectDecorators(Panel grid)
        {
            grid.Children.Add(rectTop);
            grid.Children.Add(rectLeft);
            grid.Children.Add(rectRight);
            grid.Children.Add(rectBottom);
            grid.Children.Add(rectTopLeft);
            grid.Children.Add(rectTopRight);
            grid.Children.Add(rectBottomLeft);
            grid.Children.Add(rectBottomRight);
        }
        protected virtual void initRectDecorators()
        {
            SolidColorBrush fillBrush = new SolidColorBrush(Colors.White);
            SolidColorBrush strokeBrush = new SolidColorBrush(Colors.Blue);
            double cornerSizeIncreaseFactor = 1.2;
            double sideSizeIncreaseFactor = 1.2;
            double strokeThickness = 1.0;
            double opacity = 0.0;

            rectTop.Cursor = Cursors.SizeNS;
            rectTop.VerticalAlignment = VerticalAlignment.Top;
            rectTop.HorizontalAlignment = HorizontalAlignment.Center;
            rectTop.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectTop.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectTop.Opacity = opacity;
            rectTop.Fill = fillBrush;
            rectTop.Stroke = strokeBrush;
            rectTop.StrokeThickness = strokeThickness;

            rectLeft.Cursor = Cursors.SizeWE;
            rectLeft.VerticalAlignment = VerticalAlignment.Center;
            rectLeft.HorizontalAlignment = HorizontalAlignment.Left;
            rectLeft.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectLeft.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectLeft.Opacity = opacity;
            rectLeft.Fill = fillBrush;
            rectLeft.Stroke = strokeBrush;
            rectLeft.StrokeThickness = strokeThickness;

            rectRight.Cursor = Cursors.SizeWE;
            rectRight.VerticalAlignment = VerticalAlignment.Center;
            rectRight.HorizontalAlignment = HorizontalAlignment.Right;
            rectRight.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectRight.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectRight.Opacity = opacity;
            rectRight.Fill = fillBrush;
            rectRight.Stroke = strokeBrush;
            rectRight.StrokeThickness = strokeThickness;

            rectBottom.Cursor = Cursors.SizeNS;
            rectBottom.VerticalAlignment = VerticalAlignment.Bottom;
            rectBottom.HorizontalAlignment = HorizontalAlignment.Center;
            rectBottom.Width = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectBottom.Height = RESIZABLE_BORDER_THICKNESS * sideSizeIncreaseFactor;
            rectBottom.Opacity = opacity;
            rectBottom.Fill = fillBrush;
            rectBottom.Stroke = strokeBrush;
            rectBottom.StrokeThickness = strokeThickness;

            rectTopLeft.Cursor = Cursors.SizeNWSE;
            rectTopLeft.VerticalAlignment = VerticalAlignment.Top;
            rectTopLeft.HorizontalAlignment = HorizontalAlignment.Left;
            rectTopLeft.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectTopLeft.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectTopLeft.Opacity = opacity;
            rectTopLeft.Fill = fillBrush;
            rectTopLeft.Stroke = strokeBrush;
            rectTopLeft.StrokeThickness = strokeThickness;

            rectTopRight.Cursor = Cursors.SizeNESW;
            rectTopRight.VerticalAlignment = VerticalAlignment.Top;
            rectTopRight.HorizontalAlignment = HorizontalAlignment.Right;
            rectTopRight.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectTopRight.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectTopRight.Opacity = opacity;
            rectTopRight.Fill = fillBrush;
            rectTopRight.Stroke = strokeBrush;
            rectTopRight.StrokeThickness = strokeThickness;

            rectBottomLeft.Cursor = Cursors.SizeNESW;
            rectBottomLeft.VerticalAlignment = VerticalAlignment.Bottom;
            rectBottomLeft.HorizontalAlignment = HorizontalAlignment.Left;
            rectBottomLeft.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectBottomLeft.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectBottomLeft.Opacity = opacity;
            rectBottomLeft.Fill = fillBrush;
            rectBottomLeft.Stroke = strokeBrush;
            rectBottomLeft.StrokeThickness = strokeThickness;

            rectBottomRight.Cursor = Cursors.SizeNWSE;
            rectBottomRight.VerticalAlignment = VerticalAlignment.Bottom;
            rectBottomRight.HorizontalAlignment = HorizontalAlignment.Right;
            rectBottomRight.Width = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectBottomRight.Height = RESIZABLE_BORDER_THICKNESS * cornerSizeIncreaseFactor;
            rectBottomRight.Opacity = opacity;
            rectBottomRight.Fill = fillBrush;
            rectBottomRight.Stroke = strokeBrush;
            rectBottomRight.StrokeThickness = strokeThickness;

        }
        protected virtual void setDecoratorOpacity(double opaqueValue)
        {
            rectTop.Opacity = opaqueValue;
            rectLeft.Opacity = opaqueValue;
            rectRight.Opacity = opaqueValue;
            rectBottom.Opacity = opaqueValue;
            rectTopLeft.Opacity = opaqueValue;
            rectTopRight.Opacity = opaqueValue;
            rectBottomLeft.Opacity = opaqueValue;
            rectBottomRight.Opacity = opaqueValue;

            //thumbTop.Opacity = opaqueValue;
            //thumbLeft.Opacity = opaqueValue;
            //thumbRight.Opacity = opaqueValue;
            //thumbBottom.Opacity = opaqueValue;
            //thumbTopLeft.Opacity = opaqueValue;
            //thumbTopRight.Opacity = opaqueValue;
            //thumbBottomLeft.Opacity = opaqueValue;
            //thumbBottomRight.Opacity = opaqueValue;
        }

        public Point calculateNewPosition(double xChange, double yChange)
        {
            return thumbMove.calculateNewPosition(xChange, yChange);
        }

        public void setPosition(Point newPosition)
        {
            thumbMove.setPosition(newPosition);
        }
        public void changeBorderDecoratorEdgeMode(EdgeMode edgeMode)
        {
            RenderOptions.SetEdgeMode(borderDecorator, edgeMode);
        }
    }
}
