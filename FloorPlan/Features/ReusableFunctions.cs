﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using System.IO;
using System.Text.RegularExpressions;

using Microsoft.Samples.CustomControls;

namespace FloorPlan.Features
{
    static class ReusableFunctions
    {
        public static void colorDynamicChange(SolidColorBrush brush, Window owner)
        {
            if (brush == null)
            {
                return;
            }

            Color savedColor = brush.Color;

            ColorPickerDialog colorPicker = new ColorPickerDialog();
            colorPicker.Owner = owner;

            colorPicker.StartingColor = brush.Color;
            colorPicker.dynamicallyBoundBrush = brush;

            bool? dialogResult = colorPicker.ShowDialog();
            if (dialogResult != null && (bool)dialogResult == true)
            {
                brush.Color = colorPicker.SelectedColor;
            }
            else
            {
                brush.Color = savedColor;
            }
        }


        //public static Color? queryForColor()
        //{
        //    System.Windows.Forms.ColorDialog colorDialog = ReusableDialogBoxes.colorDialog;
        //    if (colorDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        //    {
        //        Color col = new Color(); //From System.Windows.Media

        //        col.A = colorDialog.Color.A;
        //        col.R = colorDialog.Color.R;
        //        col.G = colorDialog.Color.G;
        //        col.B = colorDialog.Color.B;

        //        return col;
        //    }

        //    return null;
        //}

        public static void fillComboBoxWithFonts(ComboBox comboBoxFonts)
        {
            System.Drawing.Text.InstalledFontCollection installedFonts = new System.Drawing.Text.InstalledFontCollection();
            foreach (System.Drawing.FontFamily fontFamily in installedFonts.Families)
            {
                comboBoxFonts.Items.Add(fontFamily.Name);
            }
        }

        public static void showCommonError(Exception ex)
        {
            MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void printCommonError(Exception ex)
        {
            Console.WriteLine("EXCEPTION: " + ex.ToString());
        }

        public static void directoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    directoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public static bool deleteFolder(string path)
        {
            bool wasDeleted = false;

            if (Directory.Exists(path))
            {
                try
                {
                    Directory.Delete(path, true);
                    wasDeleted = true;
                }
                catch (Exception ex)
                {
                    showCommonError(ex);
                }
            }
            else
            {
                wasDeleted = true;
            }

            return wasDeleted;
        }

        public static String RemoveWhitespaceRepetitions(String sequence)
        {
            sequence = sequence.Trim();
            sequence = RemoveSpaceRepetitions(sequence);
            sequence = Regex.Replace(sequence, "(\r?\n )", "\n");
            sequence = Regex.Replace(sequence, "( \r?\n)", "\n");
            sequence = Regex.Replace(sequence, @"(\r?\n( )*)*\r?\n(\r?\n( )*)*", "\n"); // "\r\n" on Windows, "\n" on Mac? 

            return sequence;
        }

        public static String PlaceOnOneLine(String sequence)
        {
            sequence = sequence.Trim();
            sequence = sequence = Regex.Replace(sequence, "(\r?\n)", " ");
            sequence = RemoveSpaceRepetitions(sequence);

            return sequence;
        }

        public static String RemoveSpaceRepetitions(String sequence)
        {
            sequence = Regex.Replace(sequence, "( )*( )( )*", " ");

            return sequence;
        }

        public static void RemovePixelSnappingFromEachChild(UIElement topLevelParent)
        {
            try
            {
                int childrenCount = VisualTreeHelper.GetChildrenCount(topLevelParent);
                for (int i = 0; i < childrenCount; i++)
                {
                    UIElement child = VisualTreeHelper.GetChild(topLevelParent, i) as UIElement;
                    if (child != null)
                    {
                        child.SnapsToDevicePixels = false;
                        RemovePixelSnappingFromEachChild(child);
                    }
                }
            }
            catch (Exception ex)
            {
                printCommonError(ex);
            }
        }
    }
}
