﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FloorPlan.Logic.FileManagement;

namespace FloorPlan.Features
{
    static class ReusableDialogBoxes
    {
        public static Microsoft.Win32.OpenFileDialog openFileDlgProjects = new Microsoft.Win32.OpenFileDialog()
        {
            Title = "Select a project to open",
            Filter = "Floor Project (*" + ProjectManipulation.PROJECT_FILE_EXTENSION + ")|*" + ProjectManipulation.PROJECT_FILE_EXTENSION
        };

        public static System.Windows.Forms.ColorDialog colorDialog = new System.Windows.Forms.ColorDialog()
        {
            AllowFullOpen = true
        };

        public static Microsoft.Win32.OpenFileDialog openFileDlgImages = new Microsoft.Win32.OpenFileDialog()
        {
            Title = "Select a picture to load",
            Filter = "All files |*.*|" +
                         "Bitmap (*.bmp)|*.bmp|" +
                         "Portable Network Graphics (*.png)|*.png|" +
                         "Joint Photographic Experts Group (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                         "Graphics Interchange Format (*.gif)|*.gif|" +
                         "Tagged Image File Format (*.tiff;*.tif)|*.tiff;*.tif"
        };

        public static System.Windows.Forms.FolderBrowserDialog folderBrowserDlg = new System.Windows.Forms.FolderBrowserDialog()
        {
            Description = "Browse For Folder",
            RootFolder = Environment.SpecialFolder.MyComputer
        };
    }
}
