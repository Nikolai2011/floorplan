﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Controls;

namespace FloorPlan.Interaction.CustomValidationRules
{
    class FileNameValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            string passedValue = (string)value;

            if (passedValue == string.Empty || passedValue == null)
            {
                return new ValidationResult(false, "You must enter a name.");
            }

            if (!isValidFileName(passedValue))
            {
                return new ValidationResult(false, "The following characters are not allowed: \n/ \\ : * ? \" < > |");
            }

            return new ValidationResult(true, null);
        }

        private bool isValidFileName(string passedValue)
        {
            bool valid = true;

            if (passedValue.Contains("/") || passedValue.Contains("\\") || passedValue.Contains(":"))
            {
                valid = false;
            }
            else if (passedValue.Contains("*") || passedValue.Contains("?") || passedValue.Contains("\"") || passedValue.Contains("<"))
            {
                valid = false;
            }
            else if (passedValue.Contains(">") || passedValue.Contains("|"))
            {
                valid = false;
            }

            return valid;
        }
    }
}
