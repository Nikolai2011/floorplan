﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Controls;

namespace FloorPlan.Interaction.CustomValidationRules
{
    public class ElementSizeValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double result = 0;
            if (!double.TryParse((string)value, out result))
            {
                return new ValidationResult(false, "The value is invalid.");
            }
            else if (result < Components.Main.PhotoComponent.MIN_SIZE)
            {
                return new ValidationResult(false, "The value cannot be less than " + Components.Main.PhotoComponent.MIN_SIZE + ". The component will not be noticeable.");
            }
            else if (result > 300.0)
            {
                return new ValidationResult(false, "The value is too large. The element with this size can be inserted manually.");
            }

            return new ValidationResult(true, null);
        }
    }
}
