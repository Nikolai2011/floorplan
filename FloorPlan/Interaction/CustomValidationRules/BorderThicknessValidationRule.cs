﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Controls;

namespace FloorPlan.Interaction.CustomValidationRules
{
    public class BorderThicknessValidationRule: ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int result = 0;
            if (!int.TryParse((string)value, out result))
            {
                return new ValidationResult(false, "The value is invalid.");
            }
            else if (result > 40)
            {
                return new ValidationResult(false, "The value cannot be greater than 40.");
            }
            else if (result < 0)
            {
                return new ValidationResult(false, "The value cannot be negative.");
            }

            return new ValidationResult(true, null);
        }
    }
}
