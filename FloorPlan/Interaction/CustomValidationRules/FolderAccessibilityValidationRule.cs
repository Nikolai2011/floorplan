﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Controls;
using System.IO;

namespace FloorPlan.Interaction.CustomValidationRules
{
    public class FolderAccessibilityValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if ((string)value == string.Empty || (string)value == null)
            {
                return new ValidationResult(false, "The path cannot be empty.");
            }

            if (File.Exists((string)value))
            {
                return new ValidationResult(false, "This is not a directory.");
            }

            if (!Directory.Exists((string)value))
            {
                return new ValidationResult(false, "The specified directory does not exist.");
            }

            // path is valid and accessible
            return new ValidationResult(true, null);
        }
    }
}
