﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using System.Windows.Media;
using System.Globalization;
using System.Windows.Controls;
using System.Drawing;

namespace FloorPlan.Interaction.CustomValidationRules
{
    public class FontFamilyValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            bool valid = true;
            FontFamily fontFamilyTest = null;

            try
            {
                fontFamilyTest = new FontFamily((string)value);
            }
            catch
            {
                valid = false;
            }
            finally
            {
                if (fontFamilyTest != null)
                {
                    fontFamilyTest.Dispose();
                }
            }

            if (!valid)
            {
                return new ValidationResult(false, "Font name is not valid.");
            }

            return new ValidationResult(true, null);
        }
    }
}
