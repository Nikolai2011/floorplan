﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Controls;

namespace FloorPlan.Interaction.CustomValidationRules
{
    public class FontSizeValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double result = 0;
            if (!double.TryParse((string)value, out result))
            {
                return new ValidationResult(false, "The value is invalid.");
            }
            else if (result > 150.0)
            {
                return new ValidationResult(false, "The value cannot be greater than 150.");
            }
            else if (result < 8.0)
            {
                return new ValidationResult(false, "The value cannot be less than 8.");
            }

            return new ValidationResult(true, null);
        }
    }
}
