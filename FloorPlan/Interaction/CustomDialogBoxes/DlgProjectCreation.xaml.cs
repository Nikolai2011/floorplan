﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;

using FloorPlan.Logic.FileManagement;
using FloorPlan.Features;

namespace FloorPlan.Interaction.CustomDialogBoxes
{
    /// <summary>
    /// Interaction logic for DlgProjectCreation.xaml
    /// </summary>
    public partial class DlgProjectCreation : Window, INotifyPropertyChanged
    {
        private string _selectedProjectName = "";
        public string selectedProjectName
        {
            get { return _selectedProjectName; }
            set
            {
                if (_selectedProjectName != value)
                {
                    _selectedProjectName = value;
                    this.NotifyPropertyChanged("selectedProjectName");
                }
            }
        }

        public string directoryPathOfNewProject = null;

        public DlgProjectCreation()
        {
            InitializeComponent();

            string[] existingProjects;
            if (Directory.Exists(ProjectManipulation.FOLDER_WITH_PROJECTS))
            {
                existingProjects = Directory.GetDirectories(ProjectManipulation.FOLDER_WITH_PROJECTS);
                itemsControlExistingProjects.ItemsSource = existingProjects;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        private void windowExportOptions_Loaded(object sender, RoutedEventArgs e)
        {
            updateTextBoxValidation();
        }

        private void updateTextBoxValidation()
        {
            textBoxProjectName.Text = selectedProjectName;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (Validation.GetHasError(textBoxProjectName))
            {
                MessageBox.Show("The provided data is invalid.", "Operation Cancelled", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                directoryPathOfNewProject = ProjectManipulation.FOLDER_WITH_PROJECTS + "\\" + this.selectedProjectName;

                MessageBoxResult result = MessageBoxResult.Yes;
                if (Directory.Exists(directoryPathOfNewProject))
                {
                    result = MessageBox.Show("A directory with the same name already exists.\nDo you want to replace it with a new and empty project folder?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Yes)
                    {
                        Directory.Delete(directoryPathOfNewProject, true);
                    }
                }
                else if (File.Exists(directoryPathOfNewProject))
                {
                    result = MessageBox.Show("A file with the same name already exists.\nDo you want to replace it with a new and empty project folder?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Yes)
                    {
                        File.Delete(directoryPathOfNewProject);
                    }
                }

                if (result == MessageBoxResult.Yes)
                {
                    ProjectManipulation.createProjectTree(directoryPathOfNewProject);
                    this.DialogResult = true;
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
    }
}
