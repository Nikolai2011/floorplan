﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FloorPlan.Logic.FileManagement;

namespace FloorPlan.Interaction.CustomDialogBoxes
{
    /// <summary>
    /// Interaction logic for DlgProjectOptionsPrompt.xaml
    /// </summary>
    public partial class DlgProjectOptionsPrompt : Window
    {
        public MainWindow projectOwner;
        bool newProjectCreated = false;

        public DlgProjectOptionsPrompt()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (newProjectCreated)
            {
                if (MessageBox.Show("A new project created!\n\nDo you want to import a floor map?", "Success", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.Yes)
                {
                    projectOwner.importImage();
                }
            }
        }

        private void buttonNewProject_Click(object sender, RoutedEventArgs e)
        {
            if (projectOwner == null)
            {
                MessageBox.Show("Unable to create a new project.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.DialogResult = true;
            }
            else
            {
                bool wasCreated = ProjectManipulation.createNewProject(projectOwner);
                if (wasCreated)
                {
                    newProjectCreated = true;
                    this.DialogResult = true;
                }
            }
        }

        private void buttonExistingProject_Click(object sender, RoutedEventArgs e)
        {
            if (projectOwner == null)
            {
                MessageBox.Show("Unable to load a project.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                this.DialogResult = true;
            }

            //WindowOpenProjectsDlg openDlg = new WindowOpenProjectsDlg();
            //openDlg.Owner = this;
            //openDlg.ShowDialog();

            bool projectOpened = ProjectManipulation.openProject(projectOwner, this);
            if (projectOpened)
            {
                this.DialogResult = true;
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}
