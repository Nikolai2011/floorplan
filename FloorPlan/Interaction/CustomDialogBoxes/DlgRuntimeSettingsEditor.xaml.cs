﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

using FloorPlan.Logic.Serialization.Data;
using FloorPlan.Features;

namespace FloorPlan.Interaction.CustomDialogBoxes
{
    /// <summary>
    /// Interaction logic for DlgRuntimeSettingsEditor.xaml
    /// </summary>
    public partial class DlgRuntimeSettingsEditor : Window, INotifyPropertyChanged
    {
        private ComponentStyle _style;
        public ComponentStyle style
        {
            get { return _style; }
            set
            {
                if (_style != value)
                {
                    _style = value;
                    //this.NotifyPropertyChanged("style");
                }
            }
        }

        private SolidColorBrush photoBorderBrush;
        public Thickness photoBorderThickness
        {
            get { return style.photoBorderThickness; }
            set
            {
                if (style.photoBorderThickness != value)
                {
                    style.photoBorderThickness = value;
                    this.NotifyPropertyChanged("photoBorderThickness");
                }
            }
        }
        public double photoInitialWidth
        {
            get { return style.photoInitialSize.Width; }
            set
            {
                if (style.photoInitialSize.Width != value)
                {
                    style.photoInitialSize.Width = value;
                    this.NotifyPropertyChanged("photoInitialWidth");
                }
            }
        }
        public double photoInitialHeight
        {
            get { return style.photoInitialSize.Height; }
            set
            {
                if (style.photoInitialSize.Height != value)
                {
                    style.photoInitialSize.Height = value;
                    this.NotifyPropertyChanged("photoInitialHeight");
                }
            }
        }

        private SolidColorBrush nameBackBrush;
        private SolidColorBrush nameForeBrush;
        private SolidColorBrush nameBorderBrush;
        public Thickness nameBorderThickness
        {
            get { return style.nameBorderThickness; }
            set
            {
                if (style.nameBorderThickness != value)
                {
                    style.nameBorderThickness = value;
                    this.NotifyPropertyChanged("nameBorderThickness");
                }
            }
        }

        public bool nameIsBold
        {
            get { return style.nameIsBold; }
            set
            {
                if (value != style.nameIsBold)
                {
                    style.nameIsBold = value;
                    this.NotifyPropertyChanged("nameIsBold");
                }
            }
        }
        public bool nameIsItalic
        {
            get { return style.nameIsItalic; }
            set
            {
                if (value != style.nameIsItalic)
                {
                    style.nameIsItalic = value;
                    this.NotifyPropertyChanged("nameIsItalic");
                }
            }
        }
        public bool nameIsUnderline
        {
            get { return style.nameIsUnderline; }
            set
            {
                if (value != style.nameIsUnderline)
                {
                    style.nameIsUnderline = value;
                    this.NotifyPropertyChanged("nameIsUnderline");
                }
            }
        }
        public double nameFontSize
        {
            get { return style.nameFontSize; }
            set
            {
                if (value != style.nameFontSize)
                {
                    style.nameFontSize = value;
                    this.NotifyPropertyChanged("nameFontSize");
                }
            }
        }
        public string nameFontName
        {
            get { return style.nameFontName; }
            set
            {
                if (value != style.nameFontName)
                {
                    style.nameFontName = value;
                    this.NotifyPropertyChanged("nameFontName");
                }
            }
        }
        public bool nameIsEditOnCreation
        {
            get { return style.nameIsEditOnCreation; }
            set
            {
                if (value != style.nameIsEditOnCreation)
                {
                    style.nameIsEditOnCreation = value;
                    this.NotifyPropertyChanged("nameIsEditOnCreation");
                }
            }
        }

        public DlgRuntimeSettingsEditor(CommonSettings settings)
        {
            this.style = settings.componentCreationSettings;
            if (this.style == null)
            {
                this.style = new ComponentStyle();
            }

            InitializeComponent();

            initBrushes();
            bindColorsToBoxes();
            ReusableFunctions.fillComboBoxWithFonts(cmbFontFamilies);
        }

        private void photoChangeBorderColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReusableFunctions.colorDynamicChange(photoBorderBrush as SolidColorBrush, this);
        }
        private void nameChangeBorderColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReusableFunctions.colorDynamicChange(nameBorderBrush as SolidColorBrush, this);
        }
        private void nameChangeForeColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReusableFunctions.colorDynamicChange(nameForeBrush as SolidColorBrush, this);
        }
        private void nameChangeBackColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ReusableFunctions.colorDynamicChange(nameBackBrush as SolidColorBrush, this);
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            if (Validation.GetHasError(textBoxNameBorderSize) || Validation.GetHasError(textBoxPhotoBorderSize) ||
                Validation.GetHasError(textBoxPhotoHeight) || Validation.GetHasError(textBoxPhotoWidth) ||
                Validation.GetHasError(textBoxNameFontSize) || Validation.GetHasError(cmbFontFamilies))
            {
                MessageBox.Show("The provided data is invalid.", "Operation Cancelled", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            saveChosenColors();

            this.DialogResult = true;
        }

        private void initBrushes()
        {
            photoBorderBrush = new SolidColorBrush(this.style.photoBorderColor);

            nameBorderBrush = new SolidColorBrush(this.style.nameBorderColor);
            nameBackBrush = new SolidColorBrush(this.style.nameBackColor);
            nameForeBrush = new SolidColorBrush(this.style.nameForeColor);
        }
        private void bindColorsToBoxes()
        {
            try
            {
                borderPhotoBorderColor.Background = this.photoBorderBrush;

                borderNameBackColor.Background = this.nameBackBrush;
                borderNameBorderColor.Background = this.nameBorderBrush;
                borderNameForeColor.Background = this.nameForeBrush;
            }
            catch
            {
                //Do Nothing...
            }
        }
        private void saveChosenColors()
        {
            try
            {
                this.style.photoBorderColor = this.photoBorderBrush.Color;

                this.style.nameBorderColor = this.nameBorderBrush.Color;
                this.style.nameBackColor = this.nameBackBrush.Color;
                this.style.nameForeColor = this.nameForeBrush.Color;
            }
            catch
            {
                //Do Nothing...
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

    }
}
