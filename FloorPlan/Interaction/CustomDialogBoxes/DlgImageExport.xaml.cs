﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

using FloorPlan.Features;
using FloorPlan.Logic.FileManagement;

namespace FloorPlan.Interaction.CustomDialogBoxes
{
    /// <summary>
    /// Interaction logic for DlgImageExport.xaml
    /// </summary>
    public partial class DlgImageExport : Window, INotifyPropertyChanged
    {
        private static string savedFolderPath = String.Empty;
        private static string savedImageName = String.Empty;

        public System.Windows.Forms.FolderBrowserDialog folderBrowserDlg = ReusableDialogBoxes.folderBrowserDlg;
        public BitmapSource imageToExport = null;
        private string _selectedFolderPath = null;
        private string _selectedImageName = null;

        public DlgImageExport()
        {
            InitializeComponent();
            selectedFolderPath = savedFolderPath;
            selectedImageName = savedImageName;
        }
        private void windowExportOptions_Loaded(object sender, RoutedEventArgs e)
        {
            updateTextBoxValidation();
        }

        public string selectedFolderPath
        {
            get { return _selectedFolderPath; }
            set
            {
                if (_selectedFolderPath != value)
                {
                    _selectedFolderPath = value;
                    this.NotifyPropertyChanged("selectedFolderPath");
                }
            }
        }
        public string selectedImageName
        {
            get { return _selectedImageName; }
            set
            {
                if (_selectedImageName != value)
                {
                    _selectedImageName = value;
                    this.NotifyPropertyChanged("selectedImageName");
                }
            }
        }

        private void updateTextBoxValidation()
        {
            textBoxFolderPath.Text = selectedFolderPath;
            textBoxFileName.Text = selectedImageName;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (imageToExport == null)
            {
                MessageBox.Show("No image to save.", "No image", MessageBoxButton.OK);
                return;
            }
            else if (Validation.GetHasError(textBoxFileName) || Validation.GetHasError(textBoxFolderPath))
            {
                MessageBox.Show("The provided data is invalid.", "Operation Cancelled", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            try
            {
                BitmapEncoder encoder = null;

                //============STREAM saving==========
                //=========Lossless rendering below=========
                if (jpegCheckBox.IsChecked == true)
                {
                    encoder = new JpegBitmapEncoder();
                }
                else if (bmpCheckBox.IsChecked == true)
                {
                    encoder = new BmpBitmapEncoder();
                }
                else if (pngCheckBox.IsChecked == true)
                {
                    encoder = new PngBitmapEncoder();
                }
                else if (wdpCheckBox.IsChecked == true)
                {
                    encoder = new WmpBitmapEncoder();
                }
                else if (gifCheckBox.IsChecked == true)
                {
                    encoder = new GifBitmapEncoder();
                }
                else if (tiffCheckBox.IsChecked == true)
                {
                    encoder = new TiffBitmapEncoder();
                }

                if (encoder != null)
                {
                    encoder.Frames.Add(BitmapFrame.Create(imageToExport));
                    selectedImageName = ImageManipulation.appendFileExtension(selectedImageName, encoder);
                    bool wasSaved = ImageManipulation.exportImageToFile(textBoxFolderPath.Text, selectedImageName, encoder);
                    if (wasSaved)
                    {
                        this.DialogResult = true;
                        savedFolderPath = selectedFolderPath;
                        savedImageName = selectedImageName;
                        if (checkBoxOpenOnCreation.IsChecked == true)
                        {
                            try
                            {
                                System.Diagnostics.Process.Start(selectedFolderPath + "\\" + selectedImageName);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Unable to open file: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                        }
                    }
                    else
                    {
                        this.Title = "Image Export Options - *Not Saved";
                    }
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
        private void buttonBrowseFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (folderBrowserDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    selectedFolderPath = folderBrowserDlg.SelectedPath;
                }
            }
            catch (Exception ex)
            {
                ReusableFunctions.showCommonError(ex);
            }
        }
        private void checkBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox source = e.Source as CheckBox;
            if (source != null)
            {
                uncheckCheckBox(bmpCheckBox);
                uncheckCheckBox(gifCheckBox);
                uncheckCheckBox(jpegCheckBox);
                uncheckCheckBox(pngCheckBox);
                uncheckCheckBox(tiffCheckBox);
                uncheckCheckBox(wdpCheckBox);

                source.IsChecked = true;
            }
        }

        private void uncheckCheckBox(CheckBox cb)
        {
            if (cb != null && cb.IsChecked == true)
            {
                cb.IsChecked = false;
            }
        }
    }
}
