﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FloorPlan.Interaction.CustomCommands
{
    static class CustomCommands
    {
        public static readonly RoutedUICommand ImportImage =
   new RoutedUICommand("ImportImage", "ImportImage", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.I, ModifierKeys.Control) });

        public static readonly RoutedUICommand ExportImage =
   new RoutedUICommand("ExportImage", "ExportImage", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.E, ModifierKeys.Control) });

        public static readonly RoutedUICommand NewProject =
new RoutedUICommand("NewProject", "NewProject", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.N, ModifierKeys.Control) });

        public static readonly RoutedUICommand OpenProject =
new RoutedUICommand("OpenProject", "OpenProject", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.O, ModifierKeys.Control) });

        public static readonly RoutedUICommand SaveProject =
new RoutedUICommand("SaveProject", "SaveProject", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.S, ModifierKeys.Control) });

        public static readonly RoutedUICommand AddPhoto =
   new RoutedUICommand("AddPhoto", "AddPhoto", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.D1, ModifierKeys.Control) });

        public static readonly RoutedUICommand AddName =
   new RoutedUICommand("AddName", "AddName", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.D2, ModifierKeys.Control) });

        public static readonly RoutedUICommand AddPhotoName =
   new RoutedUICommand("AddPhotoName", "AddPhotoName", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.D3, ModifierKeys.Control) });

   //     public static readonly RoutedUICommand MultipleDraggingToggle =
   //new RoutedUICommand("MultipleDraggingToggle", "MultipleDraggingToggle", typeof(CustomCommands), new InputGestureCollection() { new KeyGesture(Key.D, ModifierKeys.Control) });
    }
}
