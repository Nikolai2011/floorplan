﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Windows.Data;
using System.Windows.Media;

namespace FloorPlan.Interaction.CustomValueConverters
{
    public class CurrentProjectPathToTitle : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string val = value as string;
            if (val != null)
            {
                return "FloorPlan - " + Path.GetFileName(val);
            }
            return "FloorPlan";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new NotImplementedException();
        }
    }
}
