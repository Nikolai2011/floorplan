﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Data;
using System.Windows;

namespace FloorPlan.Interaction.CustomValueConverters
{
    class PlusConstIntegerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double diff = -1.0;

            //Parse passed parameter as the number to add to passed value
            if (parameter != null && parameter is String)
            {
                Double.TryParse((string)parameter, out diff);
            }

            if (value is double)
            {
                value = (double)value + diff;
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
