﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Data;
using System.Windows.Controls;
using System.Windows.Media;

using FloorPlan.Components.Main;
using FloorPlan.Features;

namespace FloorPlan.Interaction.CustomValueConverters
{
    class NameToTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                TextBlock sender = values[0] as TextBlock;
                NameComponent name = values[1] as NameComponent;
                if (name == null)
                {
                    sender.Foreground = Brushes.DarkGray;
                    return "[not attached]";
                }
                else if (name.text == "")
                {
                    sender.Foreground = Brushes.DarkGray;
                    return "[empty]";
                }
                else
                {
                    sender.Foreground = Brushes.Black;
                    return ReusableFunctions.PlaceOnOneLine(name.text);
                }
            }
            catch
            {
                return String.Empty;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
