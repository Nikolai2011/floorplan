﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Data;
using System.Windows;

namespace FloorPlan.Interaction.CustomValueConverters
{
    class ThicknessToString : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int val = (int)((Thickness)value).Left;
                return val.ToString();
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                double result;
                if (double.TryParse((string)value, out result) && result >= 0.0)
                {
                    return new Thickness(result);
                }
            }

            return new Thickness(0);
        }
    }
}
