﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Data;
using System.Windows;

namespace FloorPlan.Interaction.CustomValueConverters
{
    class BorderThicknessToDouble : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Thickness)value).Left;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((double)value >= 0.0)
            {
                return new Thickness((double)value);
            }
            else
            {
                return new Thickness(0.0);
            }
        }
    }
}
